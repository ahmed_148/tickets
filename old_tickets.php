<?php
require 'includes/session.php';
$metaTitle = 'Tickets - SEOeStore Panel';
require 'includes/header.php';
?>

<!-- DataTables -->
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>



<div class="end-div container-fluid">
    <div id="list-box" class="row">
        <div class="col-md-3">
            <div class="side-menu">
                <div class="side-menu-header">Ticket Menu</div>
                <ul>
                    <li>
                        <a class="web2" href="ticket-submit.php"><i class="fa fa-ticket"></i>Submit New Ticket<span>Get help/support from our team.</span></a>
                    </li>
                    <li class="active">
                        <a class="all" href="tickets.php"><i class="fa fa-list-ul"></i>All Tickets<span>Show list of all of your tickets.<span></a>
                    </li>
                </ul>
            	</div>
            </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div id="order-box" class="panel-body">

				    <h3>Tickets <a class="btn btn-primary no-radius" href="ticket-submit.php">Submit New Ticket</a></h3>
					<hr>
<?php
$uid = $_SESSION["userid"];
$sql = mysql_query("SELECT
					*,
					tkt.`id` as `tkt_id`
					FROM `tickets` AS tkt
					LEFT JOIN `tickets_cat` AS cat ON tkt.`cat_id` = cat.`id`
					WHERE `uid` = '$uid'
					ORDER BY `tkt_id` DESC
					");
if(mysql_num_rows($sql) > 0) {

?>
					<table class="table table-striped table-hover">
		                <thead>
		                  <tr>
		                    <th>Ticket ID</th>
		                    <th>Subject</th>
		                    <th>Category</th>
		                    <th>Order ID</th>
		                    <th>Status</th>
		                    <th><?php $general ->toolTip('Last update', 'GMT')?></th>
		                  </tr>
		                </thead>
		                <tbody>


<?php
while ($row = mysql_fetch_array($sql)) {

	$statusID = $row['status_id'];

	switch ($statusID) {
		case '1':
			$status = '<span class="label label-warning">Awaiting agent</span>';
			break;
		case '2':
			$status = '<span class="label label-info">Responded</span>';
			break;
		case '3':
			$status = '<span class="label label-success">Solved</span>';
			break;
		case '4':
			$status = '<span class="label label-gray">Closed</span>';
			break;
	}

  echo '<tr><td>';
  if ($row['uread'] == 1) echo "<b>";
  echo '<a href="ticket.php?id=' . $row['tkt_id'] . '">#'.$row['tkt_id'].'</a>';
  if ($row['uread'] == 1) echo "</b>";
  echo '</td><td>';
  if ($row['uread'] == 1) echo "<b>";
  echo '<a href="ticket.php?id=' . $row['tkt_id'] . '">'.$row['subject'].'</a>';
  if ($row['uread'] == 1) echo "</b>";
  echo '</td><td>';
  echo $row['category'];
  echo '</td><td>';
  echo $row['oid'];
  echo '</td><td>';
  echo $status;
  echo '</td><td>';
	$recordDate = strtotime($row['last-activity']);
	if (gmdate("Ymd") == date("Ymd", $recordDate)){
	    $time =  date("h:i A", $recordDate);
	    $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
	}
	else {
	    $date = date("M", $recordDate) . " " . intval(date("d", $recordDate));
	    $fullDate = date("Y/m/d h:i A", $recordDate);
	    $general ->toolTipClass($date, $fullDate, 'label label-gray');
	}
  echo '</td></tr>';
}
?>
						</tbody>
					</table>
<?php
} else {
	echo "There is no tickets to display!";
}
?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
  $(function () {
    $('.table').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": false,
      "info": true,
      "autoWidth": false
    });
  });
</script>

<?php
include 'includes/footer.php';
?>