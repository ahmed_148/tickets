<div id="footer">
    <div class="container">
		<div class="row margin-top-40 margin-bottom-40">

			<div class="col-lg-3 col-md-6 col-sm-12 margin-bottom-20">
				<h3 class="margin-top-20"><b>SEO Services</b></h3>
                <p><a href="seo-campaigns.php">SEO Campaigns</a></p>
                <p><a href="order.php">Backlinks</a></p>
                <p><a href="dripfeed.php">Drip Feed</a></p>
                <p><a href="details.php">Services Details</a></p>
                <!-- <p><a href="">Pricing</a></p> -->
			</div>

			<div class="col-lg-3 col-md-6 col-sm-12 margin-bottom-20">
			  	<div>
					<h3 class="margin-top-20"><b>Billing</b></h3>
                    <p><a href="balance.php">Top-Up</a></p>
                    <p><a href="payments.php">Payment History</a></p>
                    <p><a href="coupons.php">Coupons</a></p>
				</div>
			  	<div>
					<h3 class="margin-top-40"><b>Free SEO Money</b></h3>
                    <p><a href="free_balance.php">Free Balance</a></p>
                    <p><a href="offers.php">Offers</a></p>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-12 margin-bottom-20">
				<div>
					<h3 class="margin-top-20"><b>Integrations</b></h3>
                    <p><a href="api.php">API</a></p>
                    <p><a href="api-examples.php">API Examples</a></p>
                    <!-- <p><a href="">SEO Panel Script <sup class="badge bg-gray-10">For Resellers</sup></a></p> -->
				</div>
				<div>
					<h3 class="margin-top-40"><b>Affiliate</b></h3>
                    <p><a href="affiliate.php">Affiliate Program</a></p>
                    <p><a href="affiliate-promotions.php">Promotions</a></p>
				</div>
			</div>

			<div class="col-lg-3 col-md-6 col-sm-12 margin-bottom-20">
				<div>
					<h3 class="margin-top-20"><b>Support</b></h3>
                    <p><a href="contact.php">Contact US</a></p>
                    <p><a href="tickets.php">Submit Ticket</a></p>
                    <!-- <p><a href="">Live Chat</a></p> -->
                    <p><a href="faq.php">FAQ</a></p>
				</div>
				<div>
					<h3 class="margin-top-40"><b>Get in Touch</b></h3>
					<div class="social-icons">
						<a class="fb" href="http://www.facebook.com/SEOeStore" target="_blank"><i class="fa fa-facebook"></i></a>
						<a class="tw" href="http://www.twitter.com/SEOeStore" target="_blank"><i class="fa fa-twitter"></i></a>
						<a class="gp" rel="Publisher" href="http://plus.google.com/113088264943051065922" target="_blank"><i class="fa fa-google-plus"></i></a>
					</div>
					<div class="skype">
						<i class="fa fa-skype"></i> <small>Skype ID:</small> <b>SEOeStore</b>
					</div>
				</div>
			</div>

		</div>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<p><a href="index.php"><img src="assets/img/logo-gray.png" width="100px"></a></p>
				<p><small>© 2015 - <?=date('Y')?>. All Rights Reserved.</small></p>
			</div>
			<div class="col-md-8">
				<div class="pull-right">
					<a class="margin-right-10" href="tos.php">Terms of service</a>
					<a class="margin-right-10" href="privacy-policy.php">Privacy Policy</a>
				</div>
			</div>
		</div>
	</div>
</div>



	</body>
</html>

<?php mysql_close($conn); //close mysql connection?>