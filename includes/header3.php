<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Meta -->
    <?php if(!isset($mTtl)) $mTtl = 'SEOeStore Panel'; ?>
    <title><?php echo $mTtl; ?></title>
    <?php if (isset($mDesc)) echo '<meta name="description" content="' . $mDesc . '"/>'; ?>
    <?php if (isset($mKw)) echo '<meta name="keywords" content="' . $mKw . '"/>'; ?>

    <!-- Favicon -->
    <link rel="icon" href="./favicon.ico">

    <!-- Bootstrap Stylesheet -->
    <!-- <link href="assets/css/bs4/bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

    <!-- Core Stylesheet -->
    <link href="assets/css/style3.css?v=1.1" rel="stylesheet">

    <!-- icons -->
    <link href="assets/fonts/icons/style.css" rel="stylesheet">

    <!-- Jquery-2.2.4 JS -->
    <!-- <script src="assets/js/jquery-2.2.4.min.js"></script> -->
    <!-- Bootstrap-4 Beta JS -->
    <!-- <script src="assets/js/bs4/bootstrap.min.js"></script> -->

    <!-- Jquery, Jquery slim, Popper & Bootstrap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <!-- notify -->
    <!-- <script src="./assets/js/bootstrap-notify.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/mouse0270-bootstrap-notify/3.1.7/bootstrap-notify.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css"/>

    <!-- icheck -->
    <!-- <script src="plugins/iCheck/icheck.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/icheck.min.js"></script>
    <!-- <link rel="stylesheet" href="plugins/iCheck/all.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/iCheck/1.0.2/skins/minimal/blue.css" />

    <!-- SELECT2 -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>

    <!-- core js -->
    <script src="./includes/core.js" crossorigin="anonymous"></script>

    <?php
    if (ucheck()){
        if ($user['status'] == 1){
            echo '<script>$(function(){notify("warning", "Please verify your email!","./profile.php")})</script>';
        }
    }
    ?>

    <?php
        if(isset($ogp)) echo $ogp;
        if(isset($twc)) echo $twc;
    ?>
</head>
<body>