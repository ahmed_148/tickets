    <!-- ***** START Footer Area ***** -->
    <footer class="bg-gray-90">
        <div class="container pt-7">
            <div class="row text-gray-10 lh-150">

                <div class="col-lg-3 col-md-6 col-sm-12 mb-5">
                    <div class="mb-2 f-150 b-6 d-block">SEO Services</div>
                    <ul>
                        <li><a href="seo-campaigns.php">SEO Campaigns</a></li>
                        <li><a href="order.php">Backlinks</a></li>
                        <li><a href="dripfeed.php">Drip Feed</a></li>
                        <li><a href="details.php">Services Details</a></li>
                        <!-- <li><a href="">Pricing</a></li> -->
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 mb-5">
                    <div class="mb-2 f-150 b-6 d-block">Billing</div>
                    <ul>
                        <li><a href="topup.php">Top-Up</a></li>
                        <li><a href="payments.php">Payment History</a></li>
                        <li><a href="coupons.php">Coupons</a></li>
                    </ul>
                    <div class="mt-5 mb-2 f-150 b-6 d-block">Free SEO Money</div>
                    <ul>
                        <li><a href="free-balance.php">Free Balance</a></li>
                        <li><a href="offers.php">Offers</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 mb-5">
                    <div class="mb-2 f-150 b-6 d-block">Integrations</div>
                    <ul>
                        <li><a href="api.php">API</a></li>
                        <li><a href="api-examples.php">API Examples</a></li>
                        <!-- <li><a href="">SEO Panel Script <sup class="badge bg-gray-10">For Resellers</sup></a></li> -->
                    </ul>
                    <div class="mt-5 mb-2 f-150 b-6 d-block">Affiliate</div>
                    <ul>
                        <li><a href="affiliate.php">Affiliate Program</a></li>
                        <li><a href="affiliate-promotions.php">Promotions</a></li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-12 mb-5">
                    <div class="mb-2 f-150 b-6 d-block">Support</div>
                    <ul>
                        <li><a href="contact.php">Contact Us</a></li>
                        <li><a href="tickets.php">Submit Ticket</a></li>
                        <!-- <li><a href="">Live Chat</a></li> -->
                        <li><a href="faq.php">FAQ</a></li>
                    </ul>
                    <div class="mt-5 mb-2 f-150 b-6 d-block">Get in Touch</div>
                    <ul>
                        <div class="f-150">
                            <a class="mr-1" href="https://www.facebook.com/SEOeStore" target="_blank"><i class="fi-fb"></i></a>
                            <a class="mr-1" href="https://www.twitter.com/SEOeStore" target="_blank"><i class="fi-tw"></i></a>
                            <a class="mr-1" href="https://www.youtube.com/SEOeStore/" target="_blank"><i class="fi-youtube"></i></a>
                            <a class="mr-1" href="https://www.instagram.com/seoestore/" target="_blank"><i class="fi-insta"></i></a>
                        </div>
                        <li class="mt-3">
                            <span class="mr-1 f-175 fi-sky align-middle"></span> <span>SEOeStore</span>
                        </li>
                    </ul>

                </div>

            </div>

            <hr class="bg-gray-70">

            <div class="row pb-4 mt-3 text-gray-10">
                <div class="col-md-4">
                    <a href="index.php"><img class="navbar-brand" src="assets/img/logo-gray.png" width="100px" alt="seoestore logo gray"></a>
                    <p><small>© 2015 - <?=date('Y')?>. All Rights Reserved.</small></p>
                </div>
                <div class="col-md-8">
                    <ul class="list-inline float-right">
                        <!-- <li class="list-inline-item"><a href="#">About</a></li> -->
                        <li class="list-inline-item"><a href="tos.php">Terms of service</a></li>
                        <li class="list-inline-item"><a href="privacy-policy.php">Privacy Policy</a></li>
                        <!-- <li class="list-inline-item"><a href="#">Contact</a></li> -->
                    </ul>
                </div>
            </div>
        </div>

    </footer>
    <!-- ***** END Footer Area ***** -->
    
