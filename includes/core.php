<?php
if(!isset($_SESSION)) session_start();
ob_start("ob_gzhandler");

// // https
// if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
//     $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//     header('HTTP/1.1 301 Moved Permanently');
//     header('Location: ' . $redirect);
//     exit();
// }

// retrun (login & register links)
$fileName = basename($_SERVER["SCRIPT_FILENAME"]);
$logLink = 'login.php?return='.$fileName;
$regLink = 'register.php?return='.$fileName;
if (isset($_GET['return']) AND substr( $_GET['return'], 0, 4 ) != "http"){
    $logLink = './login.php?return='.$_GET['return'];
    $regLink = './register.php?return='.$_GET['return'];
}

// logout function 
function logout(){
	unset($_COOKIE['username']);
	setcookie('username', null, -1, '/');
	session_destroy();
}

// old session username
if (isset($_SESSION['uname']) && !isset($_SESSION['username'])){
    $_SESSION['username'] = $_SESSION['uname'];
}
if (isset($_SESSION['username']) && !isset($_SESSION['uname'])){
    $_SESSION['uname'] = $_SESSION['username'];
}
if (isset($_SESSION['uname']) && $_SESSION['username'] != $_SESSION['uname']){
    logout();
}

// set session if cookie found
if(isset($_COOKIE['username']) AND !isset($_SESSION['uname'])) {
    $_SESSION['uname'] = $_COOKIE['username'];
}

// check uname & suspended
if(isset($_SESSION['uname'])) {
	$uname = $_SESSION['uname'];
    $user = users::where('username',$uname);
    if(!$user){
    	logout();
    	exit("<script>window.location.reload();</script>");
    }
    if($user['status']==3) {
    	logout();
    	exit("<script>window.location.reload();</script>");
    }
    $uid = $user['id'];
    $user = users::find($uid);
}

// last activities
if (isset($uid)){
    $x = new users;
    $x->id = $uid;
    $x->lastactivity = time();
    $x->update();
}

// ref URL
if (isset($_SERVER["HTTP_REFERER"]) && !isset($_SESSION['refURL'])) {
    $_SESSION['refURL'] = $_SERVER["HTTP_REFERER"];
}

// ref user
if(isset($_GET['ref'])) {
    $refu = model::secure($_GET['ref']);
    $refuser = users::where('username',$refu);
    if ($refuser) {
        $refId = $refuser['id'];
        setcookie('refId', $refId, time() + (86400 * 30), "/");
    }
}

// lock with redirect to login (login with return) or custom link
function lockRedirect($url = '') {
    if ($url == ''){
        global $logLink;
        $url = $logLink;
    }
	if (!isset($_SESSION['uname'])) exit("<script>document.location='".$url."';</script>");
}

// lock with login/register modal
function lockModal() {
	if (!isset($_SESSION['uname'])){
    ?>
        <div class="modal fade" role="dialog" id="lock-modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <?php userClass::showForm()?>
                    </div>
                </div>
            </div>
        </div>
        <script>
        $(function(){
        	$('#lock-modal').modal({backdrop: 'static', keyboard: false});
        });	
        </script>
    <?php		
	}
}

// check user session
function ucheck() {
    if (isset($_SESSION['uname'])){
        return true;
    }else{
        return false;
    }
}