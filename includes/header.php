<?php
ob_start("ob_gzhandler");
spl_autoload_register(function ($class) {
@include 'class/'.$class . '.php';
});
// // https
// if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
//     $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
//     header('HTTP/1.1 301 Moved Permanently');
//     header('Location: ' . $redirect);
//     exit();
// }

// logout function 
function logout(){
    unset($_COOKIE['username']);
    setcookie('username', null, -1, '/');
    session_destroy();
}

// old session username
if (isset($_SESSION['uname']) && !isset($_SESSION['username'])){
    $_SESSION['username'] = $_SESSION['uname'];
}
if (isset($_SESSION['username']) && !isset($_SESSION['uname'])){
    $_SESSION['uname'] = $_SESSION['username'];
}
if (isset($_SESSION['uname']) && $_SESSION['username'] != $_SESSION['uname']){
    logout();
}

if(isset($_COOKIE['username']) AND !isset($_SESSION['username'])) {
    $_SESSION['username'] = $_COOKIE['username'];
}
if (isset($_SESSION['username'])) {
    $user_check=$_SESSION['username'];
    // SQL Query To Fetch Complete Information Of User
    $ses_sql=mysql_query("SELECT * FROM `users` WHERE username='$user_check'");

    if(mysql_num_rows($ses_sql)!=1){
        session_destroy();
        echo "<script>window.location.reload();</script>";
    }else{
        $uRecord = mysql_fetch_array($ses_sql);
        if ($uRecord['status']==3){
            unset($_COOKIE['username']);
            setcookie('username', null, -1, '/');
            session_destroy();
            echo "<script>window.location.reload();</script>";
        }else{
            $_SESSION['userid']= $uRecord['id'];
            $_SESSION['balance']= $uRecord['balance'];
            $uexp = $uRecord['exp'];
        }
    }
}

$return = '';
if (isset($_GET['return']) AND substr( $_GET['return'], 0, 4 ) != "http"){
    $return = '?return='.$_GET['return'];
}

if (isset($_SESSION['userid']))
{
    $time=time();
    $userID = $_SESSION['userid'];
    $updateTime = mysql_query("UPDATE `users` SET lastactivity='$time' WHERE `id`='$userID'");
}

if (isset($_SERVER["HTTP_REFERER"]) && !isset($_SESSION['refURL'])) {
    $_SESSION['refURL'] = $_SERVER["HTTP_REFERER"];
}

if(isset($_GET['ref'])) {
    $refUSER = mysql_real_escape_string(htmlspecialchars($_GET['ref']));
    $sql=mysql_query("SELECT `id` FROM `users` WHERE username='$refUSER'");
    if(mysql_num_rows($sql)==1){
        $refId = mysql_fetch_array($sql)[0];
        setcookie('refId', $refId, time() + (86400 * 30), "/"); // 86400 = 1 day
    }
}

require 'config.php';
require 'includes/users_class.php';
require 'includes/general_class.php';
$general = new general();
$usersObject = new users();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
<?php if(!isset($metaTitle)){
    $metaTitle = 'SEOeStore Panel';
}
?>
    <title><?php echo $metaTitle; ?></title>

<?php
    if (isset($metaDesc)) {
        echo '<meta name="description" content="' . $metaDesc . '"/>';
    }
?>

<?php
    if (isset($metaKw)) {
        echo '<meta name="keywords" content="' . $metaKw . '"/>';
    }
?>
    <!--<link rel="stylesheet" href="assets/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- <link rel="stylesheet" href="assets/css/bootstrap.min.css"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- <link rel="shortcut icon" type="image/png" href="favicon.ico"/> -->
    <link rel="icon" type="image/png" href="./assets/img/ses_icon.png">
    

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="assets/css/style.css?v=38">
    
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
    <script src="includes/mainquery.js"></script>

    <script>
    $(function(){
        $(".select2").select2();
    });
    </script>

    <script>
        $(function() {
            var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
            
                if (pgurl.indexOf("?")>=1){
                var pgurl = pgurl.split("?");
                var pgurl = pgurl[0];
                }
            
                $("#nav li a").each(function(){
                      if($(this).attr("href") == pgurl || $(this).attr("href") == '' )
                      $(this).closest('li').addClass("active");
                })
            });
    </script>

    <script type="text/javascript">
    function selectText( containerid ) {

        var node = document.getElementById( containerid );

        if ( document.selection ) {
            var range = document.body.createTextRange();
            range.moveToElementText( node  );
            range.select();
        } else if ( window.getSelection ) {
            var range = document.createRange();
            range.selectNodeContents( node );
            window.getSelection().removeAllRanges();
            window.getSelection().addRange( range );
        }
    }
    </script>
    
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    </script>

</head>

<body>

<div class="navbar navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header logo">
            <a href="index.php"><img alt="SEOeStore Logo" src="assets/img/logo.png" title="SEOeStore panel" /></a>
        </div>

        <?php
        if (isset($_SESSION['username'])){
        ?>


        <ul id="nav" class="nav navbar-top-links navbar-right">
<?php
if ($uexp == '1'){
    $fundsIcon = '<b class="circle bg-blue"><i class="fa fa-usd" aria-hidden="true"></i></b>';
    $orderIcon = '<b class="circle bg-blue"><i class="fa fa-rocket" aria-hidden="true"></i></b>';
    $reportIcon = '<b class="circle bg-blue"><i class="fa fa-bars" aria-hidden="true"></i></b>';
    $tickettIcon = '<b class="circle bg-blue"><i class="fa fa-life-ring" aria-hidden="true"></i></b>';
}else{
    $fundsIcon = '<b class="circle bg-blue" data-toggle="tooltip" data-placement="bottom" title="Step 1: Add Funds!">1</b>';
    $orderIcon = '<b class="circle bg-blue" data-toggle="tooltip" data-placement="bottom" title="Step 2: Make Orders!">2</b>';
    $reportIcon = '<b class="circle bg-blue" data-toggle="tooltip" data-placement="bottom" title="Step 1: Check Reports!">3</b>';
    $tickettIcon = '';
}
?>
            <li>
                <a href="topup.php"><?php echo $fundsIcon;?> ADD FUNDS</a>
            </li>

            <li class="dropdown account">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                    <?php echo $orderIcon;?> MAKE ORDER <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="seo-campaigns.php">SEO Campaigns</a>
                    </li>
                    <li>
                        <a href="order.php">Single Order</a>
                    </li>
                    <li>
                        <a href="dripfeed.php">Drip Feed</a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a href="details.php">Services Details</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->

            <li>
            <a href="reports.php"><?php echo $reportIcon;?> REPORTS
            </a>
            </li>


            <li>
            <a href="tickets.php"><?php echo $tickettIcon;?> TICKETS
            </a>
            </li>

            <li>
            </li>

            <li class="dropdown account">
                <a class="dropdown-toggle text-black" data-toggle="dropdown" href="#" aria-expanded="false">
                    <b class="circle bg-dark-gray"><i class="fa fa-user fa-fw"></i></b>
                    <span class="text-uper"><b><?php echo $_SESSION['username'] ?></b> (<b id="user-balance" class="text-green">$<?php echo $_SESSION['balance']/100; ?></b>)</span>
                    <i class="fa fa-caret-down"></i>

                    <span id="user-balance" class="label bg-green"></span>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li><a href='balance.php'>BALANCE <b class="text-green">$<?php echo $_SESSION['balance']/100; ?></b></a></li>
                    <li class="divider"></li>
                    <li><a href="profile.php"><i class="fa fa-user"></i> User Profile</a></li>
                    <li><a href="payments.php"><i class="fa fa-history"></i> Payment History</a></li>

                    <li><a href="api.php"><i class="fa fa-code" aria-hidden="true"></i> API</a></li>
                    <li><a href="affiliate.php"><i class="fa fa-share-alt" aria-hidden="true"></i> Affiliate Program</a></li>
                    <li><a href="offers.php"><i class="fa fa-gift" aria-hidden="true"></i> Offers</a></li>
                    <li><a href="coupons.php"><i class="fa fa-tag" aria-hidden="true"></i> Coupons</a></li>
                    <li><a href="free_balance.php"><i class="fa fa-usd" aria-hidden="true"></i> Free Balance</a></li>
                    <li><a href="faq.php"><i class="fa fa-question-circle" aria-hidden="true"></i> FAQ</a></li>
                    </li>
                    <li class="divider"></li>
                    <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->

            </ul>

        <?php
        }else{
        ?>
        <div class="navbar-right nav-buttons">
            <a class="btn btn-success" href="seo-campaigns.php">SEO campaigns</a>
            <a class="btn btn-primary" href="login.php<?php echo $return;?>">Login</a>
            <a class="btn btn-default" href="register.php<?php echo $return;?>">Register</a>
        </div>
        <?php } ?>
    </div>
</div>
<div class="end-nav"></div>

<?php
if (isset($_SESSION['username'])) {    
    $usersObject->setUsername ($_SESSION['username']);
    if ($usersObject->CheckUserStatus()==1) {
        echo '<div class="container-fluid">';
        $general ->alert('<form action="profile.php" method="post">Your email address is not verified, verify it & enjoy all the panel features! <input class="btn btn-warning" type="submit" name="Verify_email" value="Verify your email now!" /></form>' , 'info');
        echo '</div>';
    }
}
echo '<div class="container-fluid">';
//$general ->alert('We are currently in Eid vacation, order delivery may take a bit longer time than normal [about 48 hrs.] Happy Eid ^^ <a href="http://bit.ly/SEOeStoreEid" target="_blank">bit.ly/SEOeStoreEid</a>', 'warning');
echo '</div>';
?>