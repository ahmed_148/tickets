		<div id="footer">
		    <div class="container-fluid">
				<div class="row">
				  <div class="col-md-4 col-sm-4">

					<p class="margin-top-20"><span>SEOeStore Panel</span> <span class="label bg-blue">v<?php echo $panelVersion ?></span></p>
					<p>
						<i class="fa fa-rocket icon-20" aria-hidden="true"></i> <a href="seo-campaigns.php">SEO Campaigns</a><br>
						<i class="fa fa-share-alt icon-20" aria-hidden="true"></i> <a href="affiliate.php">Affiliate Program</a><br>
						<i class="fa fa-hand-pointer-o icon-20" aria-hidden="true"></i> <a href="affiliate-promotions.php">Promotions</a><br>
						<i class="fa fa-code icon-20" aria-hidden="true"></i> <a href="api.php">API</a><br>
						<i class="fa fa-code icon-20" aria-hidden="true"></i> <a href="buy-seo-panel-script.php">SEO Panel Script</a><br>
						<i class="fa fa-gift icon-20" aria-hidden="true"></i> <a href="offers.php">Offers</a><br>
						<i class="fa fa-tag icon-20" aria-hidden="true"></i> <a href="coupons.php">Coupons</a><br>
						<i class="fa fa-usd icon-20" aria-hidden="true"></i> <a href="free_balance.php">Free Money</a><br>
						<i class="fa fa-question-circle icon-20" aria-hidden="true"></i> <a href="faq.php">FAQ</a><br>
						<i class="fa fa-list icon-20" aria-hidden="true"></i> <a href="tos.php">Terms of service</a><br>
						<i class="fa fa-lock icon-20" aria-hidden="true"></i> <a href="privacy-policy.php">Privacy policy</a>
					</p>
					
					<p><i class="fa fa-clock-o"></i> <u>Working hours:</u><br>Saturday to Thursday - 8AM to 3PM GMT.
					</p>

					&copy 2015 - <?php echo date('Y');?>. All Rights Reserved.
				  </div>
				  <div class="col-md-4 col-sm-4">
				  	<div class="clearfix"></div>
				    <h3>Get in Touch</h3>
					<div class="social-icons">
						<a class="fb" href="http://www.facebook.com/SEOeStore" target="_blank"><i class="fa fa-facebook"></i></a>
						<a class="tw" href="http://www.twitter.com/SEOeStore" target="_blank"><i class="fa fa-twitter"></i></a>
						<a class="gp" rel="Publisher" href="http://plus.google.com/113088264943051065922" target="_blank"><i class="fa fa-google-plus"></i></a>
					</div>
					<div class="skype">
						<i class="fa fa-skype"></i> <small>Skype ID:</small> <b>SEOeStore</b>
					</div>
				  </div>

				  <div class="col-md-4 col-sm-4">
					  <div class="clearfix"></div>
					  <img alt="paypal" class="margin-top-20 img-responsive" src="assets/img/paypal-logo.png">
				  </div>
				</div>

			</div>
		</div>

<?php
// if (isset($uid)){
// 	$uexp = user::find($uid)['exp'];
// 	if ($uexp != 1){
?>
<!-- 	<div id="faq-icon">
		<a class="tip" data-toggle="tooltip" data-placement="top" title="" data-original-title="FAQ" href="faq.php"><img class="img-responsive" src="assets/img/faq.png"></a>
	</div> -->
<?php
// 	}
// }else{
?>
<!-- 	<div id="faq-icon">
		<a class="tip" data-toggle="tooltip" data-placement="top" title="" data-original-title="FAQ" href="faq.php"><img class="img-responsive" src="assets/img/faq.png"></a>
	</div> -->

<?php
// }
?>
		<script src="assets/js/classie.js"></script>
    	<script src="assets/js/cbpAnimatedHeader.js"></script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-65569539-1', 'auto');
		  ga('send', 'pageview');

		</script>

		<!-- Google Code for Remarketing Tag -->
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 870009939;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/870009939/?guid=ON&amp;script=0"/>
		</div>
		</noscript>
		<!-- End Google Code for Remarketing Tag -->


<!-- <div class="alert-buttom">
	<div class="alert alert-dismissible margin-bottom-0" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<a href="birthday-offers.php">
			<img src="assets/img/birthday.png" width="200px" alt="gift">
		</a>
	</div>
</div>

<div class="alert-buttom">
  <div class="alert alert-dismissible margin-bottom-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <a href="spring_deals.php">
      <img src="assets/img/r-easter.png" width="150px" alt="gift">
    </a>
  </div>
</div>


		<div class="alert-buttom">
	        <div class="alert alert-dismissible margin-bottom-0" role="alert">
	          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <a href="support_btc.php">
	              <img src="assets/img/btc_support.png" width="150px" alt="gift">
	            </a>
	        </div>
	    </div>
	-->

<!--
		<div class="alert-buttom">
		    <div class="alert bg-yellow alert-dismissible" role="alert">
		      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <a href="deals.php">
		            <b>Cyber Monday Deals</b>
		            (Clear, Automated, and Unlimited)!
		        </a>
		    </div>
		    <div class="alert bg-yellow alert-dismissible" role="alert">
		      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <a href="buy-seo-panel-script.php?promo=BlackFriday">
		            <b>SEO Panel script with 62.5% off</b>
		            For just <span style="text-decoration:line-through">$250</span> <b>$97</b> start your 100% automated business
		        </a>
		    </div>
		</div>

		<div class="alert-buttom">
		    <div class="alert alert-dismissible margin-bottom-0" role="alert">
		      <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		        <a href="new-year2018.php">
		          <img src="assets/img/gift.png" width="100px" alt="gift">
		        </a>
		    </div>
		</div> 
-->

<!-- <div id="popup-eggs" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class=" text-center">
      <button type="button" class="close custom-close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <a href="spring_deals.php">
        <img src="assets/img/pop_spring.png" alt="">
      </a>
    </div>
  </div>
</div> -->


<?php
// start popup
//if(isset($uid)){
	//$query = mysql_query("SELECT * FROM `popup` WHERE `uid` = '$uid' AND `popid` = '1'");
	//if (mysql_num_rows($query)==0){
		//$query = mysql_query("INSERT INTO `popup`(`popid`, `uid`, `repeat`) VALUES ('1','$uid','1')");
		?>
<script>
// When the user clicks on <div>, open the popup
//$(document).ready(function(){
    //$("#popup-eggs").modal();
//});
</script>
		<?php
	//}
//}else{
	?>
<script>
// When the user clicks on <div>, open the popup
//$(document).ready(function(){
    //$("#popup-eggs").modal();
//});
</script>
	<?php
//}
// end popup
?>


<!-- <div class="alert-buttom">
	<div class="alert alert-dismissible margin-bottom-0" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<a href="fun-week.php">
			<img src="assets/img/minions-2.png" width="200px" alt="gift">
		</a>
	</div>
</div> -->


<?php
if (isset($uid)){
	$query = mysql_query("SELECT * FROM `fnl_core` WHERE `uid`='$uid' AND (`status_id`=0 OR `status_id`=1 OR `status_id`=2)");
	if(mysql_num_rows($query)>0){
		$fnlCoreAvailable = mysql_fetch_array($query);
		$fnlCoreHash = $fnlCoreAvailable['hash'];
		$fnlCoreType = $fnlCoreAvailable['type'];
		if ($fnlCoreType == 1){
			$fnlCoreLink = './fnl-offer.php?id=';
		}
		if ($fnlCoreType == 2){
			$fnlCoreLink = './fnl-dripfeed.php?id=';
		}
	?>
 	<div class="alert-buttom">
		<div class="alert alert-dismissible margin-bottom-0" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			</button>
			<a href="<?=$fnlCoreLink.$fnlCoreHash?>">
				<img src="assets/img/special-offer.png" width="100px" alt="Special Offer">
			</a>
		</div>
	</div>
<?php }} ?>


<!-- <div class="alert-buttom">
  <div class="alert alert-dismissible margin-bottom-0" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
    <a href="./black-friday.php">
      <img src="assets/img/cyber-monday2.png" width="150px" alt="cyber monday deals">
    </a>
  </div>
</div> -->


	</body>
</html>

<?php mysql_close($conn); //close mysql connection?>