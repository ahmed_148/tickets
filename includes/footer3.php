
    <!-- modal1 -->
    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-body p-3">
                <div class="text-center">
                    <div class="loader m-auto"></div>
                    Loading...
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- END modal1 -->

    <!-- modal1 Lock-->
    <div class="modal fade" role="dialog" id="modal-lock" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
          <div class="modal-content" >
            <div class="modal-body p-3">
                <div class="text-center">
                    <div class="loader m-auto"></div>
                    Loading...
                </div>
            </div>
          </div>
        </div>
    </div>
    <!-- END modal Lock-->

    <!-- result modal -->
    <div class="modal fade" id="result-modal" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <div class="pt-3 text-center">
                <h5>RESULT</h5><div class="line-shape bg-blue line-shape-center"></div>
            </div>
            <div class="modal-body p-3">
                <div class="text-center">
                    <div class="loader m-auto"></div>
                    Loading...
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- END result modal -->

    <!-- loading modal -->
    <div class="modal fade mt-5" role="dialog" id="loading-modal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-sm">
            <div class="modal-content p-2 text-center" >
                <div class="loader m-auto"></div>
                Loading...
            </div>
        </div>
    </div>
    <!-- END loading modal -->

</body>

</html>
