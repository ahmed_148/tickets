<!-- ***** Header Area Start ***** -->
<header class="container">
    <div class="row pt-4">
        <div id="logo" class="col-lg-2 col-sm-12 pb-2 text-lg-left text-center">    <a href="index.php"><img src="assets/img/logo.png" alt="SEOeStore logo"></a>
        </div>
        <div class="col-lg-7 col-sm-12 z-index-99">
            <nav id="top-menu">
                <label for="drop" class="toggle text-center">&#8801; Menu</label>
                <input type="checkbox" id="drop" />
                <ul class="menu">

                    <li><a href="topup.php">Add Funds</a></li>

                    <li>
                    <label for="drop-2" class="toggle">New Order &#x25BE;</label>
                    <a href="#">New Order</a>
                    <input type="checkbox" id="drop-2"/>
                    <ul class="shadow">
                        <li><a href="seo-campaigns.php">SEO Campaigns</a></li>
                        <li><a href="order.php">Backlinks</a></li>
                        <li><a href="order.php#tier-all">Link pyramid</a></li>
                        <li><a href="dripfeed.php">Drip Feed</a></li>
                        
<!-- 
                        <li>
                            <label for="drop-3" class="toggle">3 &#x25BE;</label>
                            <a href="#">3</a>         
                            <input type="checkbox" id="drop-3"/>
                            <ul>
                                <li><a href="#">31</a></li>
                                <li><a href="#">32</a></li>
                                <li><a href="#">33</a></li>
                            </ul>
                        </li>
 -->
                    </ul>
                    </li>
                    <li>
                        <a href="reports.php">Reports
                        </a>
                    </li>
                    <li>
                        <a href="tickets.php">Tickets 
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <div id="header-user float-right" class="col-lg-3 mt-2 text-right">

<?php if(ucheck()) {
?>
             <div class="dropdown mb-2">
              <button class="btn btn-outline-info dropdown-toggle w-100" type="button" id="usermenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fi-user pr-1"></i>
                <span class="text-uper"><?=$user['username']?> (<span id="user-balance" class="">$<?=$user['balance']/100?></span>)</span>
              </button>
              <div class="dropdown-menu w-100 shadow" aria-labelledby="usermenu">
                <a class="dropdown-item pt-3 pb-3" href="topup.php">Balance: $<?=$user['balance']/100?> <span class="badge bg-red">Top up</span></a>
                <!-- <a class="dropdown-item pt-3 pb-3" href="payments.php">Payment History</a> -->
                <div class="dropdown-divider"></div>
                <a class="dropdown-item pt-3 pb-3" href="profile.php">Profile</a>
                <a class="dropdown-item pt-3 pb-3" href="./logout.php">Logout</a>
              </div>
            </div>
<?php
}else{
?>
            <a href="<?=$logLink?>" class="btn btn-outline-secondary d-block d-lg-inline-block mb-2">Login</a>
            <a href="<?=$regLink?>" class="btn btn btn-success d-block d-lg-inline-block mb-2">Register</a>
<?php
}
?>

        </div>
    </div>
</header>
<!-- ***** Header Area End ***** -->


