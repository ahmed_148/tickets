<?php
    require './classes/db.php';
    require './classes/model.php';
    require './classes/tables.php';
    require './classes/user.php';
    require './includes/core.php';
    require 'includes/header3.php';
    require 'includes/nav.php';
?>

<div class="container">

    <hr>
    <h3>headings</h3>
    <xmp><h1>header H1</h1></xmp>
    <h1>header H1</h1>
    <h2>header H2</h2>
    <h3>header H3</h3>
    <h4>header H4</h4>
    <h5>header H5</h5>
    <h6>header H6</h6>

    <hr>
    <h3>font bold</h3>
    <div class="font-weight-bold">Bold text.</div>
    <div class="font-weight-light">Light text.</div>
    <div class="font-weight-normal">Normal weight text.</div>
    <div><b>bold tag</b></div>
    <div>normal font</div>
    <br>
    <xmp><div class="b-6">class: b-6 font-weight: 600</div></xmp>
    <div class="b-6">class: b-6 font-weight: 600</div>
    <div class="b-8">class: b-8 font-weight: 800</div>

    <hr>
    <h3>font size</h3>
    <xmp><div class="f-50">class: f-50</div></xmp>
    <div class="f-50">class: f-50</div>
    <div class="f-60">class: f-60</div>
    <div class="f-70">class: f-70</div>
    <div class="f-80">class: f-80</div>
    <div class="f-90">class: f-90</div>
    <div class="f-100">class: f-100</div>
    <div class="f-125">class: f-125</div>
    <div class="f-150">class: f-150</div>
    <div class="f-175">class: f-175</div>
    <div class="f-200">class: f-200</div>
    <div class="f-250">class: f-250</div>
    <div class="f-300">class: f-300</div>
    <div class="f-400">class: f-400</div>
    <div class="f-500">class: f-500</div>
    <div class="f-600">class: f-600</div>
    <div class="f-700">class: f-700</div>
    <div class="f-800">class: f-800</div>

    <hr>
    <h3>colors</h3>
    <xmp><div class="text-blue">text-blue <a href="#">link</a></div></xmp>
    <div class="text-blue">text-blue <a href="#">link</a></div>
    <div class="text-aqua">text-aqua <a href="#">link</a></div>
    <div class="text-yellow">text-yellow <a href="#">link</a></div>
    <div class="text-red">text-red <a href="#">link</a></div>
    <div class="text-orange">text-orange <a href="#">link</a></div>
    <div class="text-black">text-black <a href="#">link</a></div>
    <div class="text-light-blue">text-light-blue <a href="#">link</a></div>
    <div class="text-green">text-green <a href="#">link</a></div>
    <div class="text-gray-05">text-gray-05 <a href="#">link</a></div>
    <div class="text-gray-10">text-gray-10 <a href="#">link</a></div>
    <div class="text-gray-30">text-gray-30 <a href="#">link</a></div>
    <div class="text-gray-70">text-gray-70 <a href="#">link</a></div>
    <div class="text-gray-90">text-gray-90 <a href="#">link</a></div>
    <div class="text-fuchsia">text-fuchsia <a href="#">link</a></div>
    <div class="text-purple">text-purple <a href="#">link</a></div>
    <div class="text-light-purple">text-light-purple <a href="#">link</a></div>
    <div class="text-maroon">text-maroon <a href="#">link</a></div>
    <div class="text-white"><span class="bg-black">text-white <a href="#">link</a></span></div>

    <hr>
    <h3>backgrounds</h3>
    <xmp><div class="bg-blue">bg-blue</div></xmp>
    <div><span class="bg-blue">bg-blue</span></div>
    <div><span class="bg-aqua">bg-aqua</span></div>
    <div><span class="bg-yellow">bg-yellow</span></div>
    <div><span class="bg-red">bg-red</span></div>
    <div><span class="bg-orange">bg-orange</span></div>
    <div><span class="bg-black">bg-black</span></div>
    <div><span class="bg-light-blue">bg-light-blue</span></div>
    <div><span class="bg-green">bg-green</span></div>
    <div><span class="bg-gray-05">bg-gray-05</span></div>
    <div><span class="bg-gray-10">bg-gray-10</span></div>
    <div><span class="bg-gray-30">bg-gray-30</span></div>
    <div><span class="bg-gray-70">bg-gray-70</span></div>
    <div><span class="bg-gray-90">bg-gray-90</span></div>
    <div><span class="bg-fuchsia">bg-fuchsia</span></div>
    <div><span class="bg-purple">bg-purple</span></div>
    <div><span class="bg-light-purple">bg-light-purple</span></div>
    <div><span class="bg-maroon">bg-maroon</span></div>
    <div><span class="bg-white">bg-white</span></div>

    <hr>
    <h3>gradient backgrounds</h3>
    <xmp><div class="bg-blue-g">bg-blue-g</div></xmp>
    <div class="bg-blue-g">bg-blue-g<br><br><br></div>
    <div class="bg-aqua-g">bg-aqua-g<br><br><br></div>
    <div class="bg-yellow-g">bg-yellow-g<br><br><br></div>
    <div class="bg-red-g">bg-red-g<br><br><br></div>
    <div class="bg-orange-g">bg-orange-g<br><br><br></div>
    <div class="bg-green-g">bg-green-g<br><br><br></div>
    <div class="bg-gray-g">bg-gray-g<br><br><br></div>
    <div class="bg-light-gray-g">bg-light-gray-g<br><br><br></div>
    <div class="bg-purple-g">bg-purple-g<br><br><br></div>
    <div class="bg-blue-green-g">bg-blue-green-g<br><br><br></div>

    <hr>
    <h3>line height</h3>
    <xmp><div class="lh-200">class: lh-200 (line hight 2)<br>new line</div></xmp>

    <div class="lh-100">class: lh-100 (line hight 1)<br>new line</div>
    <div class="lh-150">class: lh-150 (line hight 1.5)<br>new line</div>
    <div class="lh-200">class: lh-200 (line hight 2)<br>new line</div>
    <div class="lh-250">class: lh-250 (line hight 2.5)<br>new line</div>
    <div class="lh-300">class: lh-300 (line hight 3)<br>new line</div>

    <hr>
    <h3>spaces</h3>
    <xmp><div class="pt-6">class: pt-6</div></xmp>
    <div class="pt-6">class: pt-6</div>
    <div>new spacing classes added:</div>
    <div>pt-6 : pt-10 and pb-6 : pb-10</div>
    <div>mt-6 : mt-10 and mb-6 : mb-10</div>

    <hr>
    <h3>boxes</h3>
<xmp><div class="row">
    <div class="col-md-3">
        <a href="#" class="info-box bg-gray-05">
          <span class="info-box-icon bg-blue-g"><i class="fi-list1"></i></span>
          <div class="info-box-content text-gray-90">
            Some Text
            <div class="b-6">0</div>
          </div>
        </a>
    </div>
</div></xmp>
    <br>
    <div class="row">
        <div class="col-md-3">
            <a href="#" class="info-box bg-gray-05">
              <span class="info-box-icon bg-blue-g"><i class="fi-list1"></i></span>
              <div class="info-box-content text-gray-90">
                Some Text
                <div class="b-6">0</div>
              </div>
            </a>
        </div>
    </div>

<br>
<xmp><div class="col-md-3">
    <div class="plan">
      <div class="f-150 text-aqua">25% Bonus</div>
      <div class="f-200 b-6 text-yellow">$125</div>
      <div>Pay ONLY <b class="bold">$100</b></div>
      <hr>
      <div class="text-gray-30">
        <small class="">Pay Only $100<br>
        Get $125 in Your Balance</small>
      </div>
      <a href="#" class="btn btn-block btn-warning mt-3">Pay ONLY <b>$100</b></a>
    </div>
</div>
<div class="col-md-3">
    <div class="plan plan-highlight">
      <div class="plan-recommended bg-aqua">Most Popular</div>
      <div class="f-150 text-aqua">25% Bonus</div>
      <div class="f-200 b-6 text-yellow">$125</div>
      <div>Pay ONLY <b class="bold">$100</b></div>
      <hr>
      <div class="text-gray-30">
        <small class="">Pay Only $100<br>
        Get $125 in Your Balance</small>
      </div>
      <a href="#"><span class="btn btn-block bg-aqua mt-3">Pay ONLY <b>$100</b></span></a>
    </div>
</div></xmp>
<br>
    <div class="row">
        <div class="col-md-3">
            <div class="plan">
              <div class="f-150 text-aqua">25% Bonus</div>
              <div class="f-200 b-6 text-yellow">$125</div>
              <div>Pay ONLY <b class="bold">$100</b></div>
              <hr>
              <div class="text-gray-30">
                <small class="">Pay Only $100<br>
                Get $125 in Your Balance</small>
              </div>
              <a href="#" class="btn btn-block btn-warning mt-3">Pay ONLY <b>$100</b></a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="plan plan-highlight">
              <div class="plan-recommended bg-aqua">Most Popular</div>
              <div class="f-150 text-aqua">25% Bonus</div>
              <div class="f-200 b-6 text-yellow">$125</div>
              <div>Pay ONLY <b class="bold">$100</b></div>
              <hr>
              <div class="text-gray-30">
                <small class="">Pay Only $100<br>
                Get $125 in Your Balance</small>
              </div>
              <a href="#"><span class="btn btn-block bg-aqua mt-3">Pay ONLY <b>$100</b></span></a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="plan plan-light">
              <div class="f-150 text-aqua">25% Bonus</div>
              <div class="f-200 b-6 text-yellow">$125</div>
              <div>Pay ONLY <b class="bold">$100</b></div>
              <hr>
              <div class="text-gray-30">
                <small class="">Pay Only $100<br>
                Get $125 in Your Balance</small>
              </div>
              <a href="#"><span class="btn btn-block bg-aqua mt-3">Pay ONLY <b>$100</b></span></a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="plan plan-light-highlight">
              <div class="plan-recommended bg-yellow">Most Popular</div>
              <div class="f-150 text-aqua">25% Bonus</div>
              <div class="f-200 b-6 text-yellow">$125</div>
              <div>Pay ONLY <b class="bold">$100</b></div>
              <hr>
              <div class="text-gray-30">
                <small class="">Pay Only $100<br>
                Get $125 in Your Balance</small>
              </div>
              <a href="#"><span class="btn btn-block bg-yellow mt-3">Pay ONLY <b>$100</b></span></a>
            </div>
        </div>
    </div>

<br>
<xmp><div class="box box-red p-2">
    TST
</div></xmp>
<br>
    <div class="row">
        <div class="col">
            <div class="box box-red p-2">
                box box-red
            </div>
        </div>
        <div class="col">
            <div class="box box-yellow p-2">
                box-yellow
            </div>
        </div>
        <div class="col">
            <div class="box box-aqua p-2">
                box-aqua
            </div>
        </div>
        <div class="col">
            <div class="box box-blue p-2">
                box-blue
            </div>
        </div>
        <div class="col">
            <div class="box box-light-blue p-2">
                box-light-blue
            </div>
        </div>
        <div class="col">
            <div class="box box-green p-2">
                box-green
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="box box-orange p-2">
                box-orange
            </div>
        </div>
        <div class="col">
            <div class="box box-gray-05 p-2">
                box-gray-05
            </div>
        </div>
        <div class="col">
            <div class="box box-gray-10 p-2">
                box-gray-10
            </div>
        </div>
        <div class="col">
            <div class="box box-gray-30 p-2">
                box-gray-30
            </div>
        </div>
        <div class="col">
            <div class="box box-gray-70 p-2">
                box-gray-70
            </div>
        </div>
        <div class="col">
            <div class="box box-gray-90 p-2">
                box-gray-90
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="box box-black p-2">
                box-black
            </div>
        </div>
        <div class="col">
            <div class="box box-fuchsia p-2">
                box-fuchsia
            </div>
        </div>
        <div class="col">
            <div class="box box-purple p-2">
                box-purple
            </div>
        </div>
        <div class="col">
            <div class="box box-light-purple p-2">
                box-light-purple
            </div>
        </div>
        <div class="col">
            <div class="box box-maroon p-2">
                box-maroon
            </div>
        </div>
        <div class="col">
            <div class="box box-solid p-2">
                box-solid
            </div>
        </div>
    </div>

<br>
<xmp><div class="col">
    <div class="border p-2 rounded-xs shadow">
        <div class="row">
            <div class="col-8">This is sample text<br>New line</div>
            <div class="col-4"><span class="float-right p-3 bg-aqua rounded-xs">TXT/ICO</span></div>
        </div>
    </div>
</div></xmp>
<br>
    <div class="row">
        <div class="col">
            <div class="border p-2 rounded-xs shadow">
                <div class="row">
                    <div class="col-8">This is sample text<br>New line</div>
                    <div class="col-4"><span class="float-right p-3 bg-aqua rounded-xs">TXT/ICO</span></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border p-2">
                <div class="row">
                    <div class="col-8">This is sample text<br>New line</div>
                    <div class="col-4"><span class="float-right p-3 bg-orange">TXT/ICO</span></div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="border p-2">
                <div class="row">
                    <div class="col-8">This is sample text<br>New line</div>
                    <div class="col-4"><span class="float-right p-3 bg-blue">TXT/ICO</span></div>
                </div>
            </div>
        </div>
    </div>

    <hr>
    <h3>tables</h3>
<xmp><table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>abc</th>
            <th>def</th>
            <th>ghi</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>
                abc1
            </td>
            <td>
                abc1
            </td>
            <td>
                abc1
            </td>
        </tr>
        <tr>
            <td>
                abc2
            </td>
            <td>
                abc2
            </td>
            <td>
                abc2
            </td>
        </tr>
        <tr>
            <td>
                abc3
            </td>
            <td>
                abc3
            </td>
            <td>
                abc3
            </td>
        </tr>
    </tbody>
</table></xmp>
    <br>
    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>abc</th>
                <th>def</th>
                <th>ghi</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    abc1
                </td>
                <td>
                    abc1
                </td>
                <td>
                    abc1
                </td>
            </tr>
            <tr>
                <td>
                    abc2
                </td>
                <td>
                    abc2
                </td>
                <td>
                    abc2
                </td>
            </tr>
            <tr>
                <td>
                    abc3
                </td>
                <td>
                    abc3
                </td>
                <td>
                    abc3
                </td>
            </tr>
        </tbody>
    </table>

    <hr>
    <h3>alerts</h3>
<xmp><div class="alert alert-primary alert-dismissible fade show" role="alert">
  <b>alert-primary</b> You should check in on some of those fields below.
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div></xmp>
    <br>
    <div class="alert alert-primary alert-dismissible fade show" role="alert">
      <b>alert-primary</b> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
      <b>alert-danger</b> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="alert alert-info alert-dismissible fade show" role="alert">
      <b>alert-info</b> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
      <b>alert-warning</b> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="alert alert-success alert-dismissible fade show" role="alert">
      <b>alert-success</b> You should check in on some of those fields below.
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>

    <hr>
    <h3>badges</h3>
<xmp><span class="badge badge-primary">Primary</span></xmp>
    <br>
    <span class="badge badge-primary">Primary</span>
    <span class="badge badge-secondary">Secondary</span>
    <span class="badge badge-success">Success</span>
    <span class="badge badge-danger">Danger</span>
    <span class="badge badge-warning">Warning</span>
    <span class="badge badge-info">Info</span>
    <span class="badge badge-light">Light</span>
    <span class="badge badge-dark">Dark</span>
    <br>
    <span class="badge bg-red">bg-red</span>
    <span class="badge bg-yellow">bg-yellow</span>
    <span class="badge bg-aqua">bg-aqua</span>
    <span class="badge bg-blue">bg-blue</span>
    <span class="badge bg-light-blue">bg-light-blue</span>
    <span class="badge bg-green">bg-green</span>
    <span class="badge bg-orange">bg-orange</span>
    <span class="badge bg-gray-05">bg-gray-05</span>
    <span class="badge bg-gray-10">bg-gray-10</span>
    <span class="badge bg-gray-30">bg-gray-30</span>
    <span class="badge bg-gray-70">bg-gray-70</span>
    <span class="badge bg-gray-90">bg-gray-90</span>
    <span class="badge bg-black">bg-black</span>
    <span class="badge bg-fuchsia">bg-fuchsia</span>
    <span class="badge bg-purple">bg-purple</span>
    <span class="badge bg-light-purple">bg-light-purple</span>
    <span class="badge bg-maroon">bg-maroon</span>
    <a href=""><span class="badge bg-aqua">link bg-aqua</span></a>

    <hr>
    <h3>buttons</h3>
    <xmp><button type="button" class="btn bg-orange icon-box icon-box-5 rounded-circle"><i class="fi-list1"></i></button></xmp>
    <br>
    <button type="button" class="btn btn-primary">Primary</button>
    <button type="button" class="btn btn-secondary">Secondary</button>
    <button type="button" class="btn btn-success">Success</button>
    <button type="button" class="btn btn-danger">Danger</button>
    <button type="button" class="btn btn-warning">Warning</button>
    <button type="button" class="btn btn-info">Info</button>
    <button type="button" class="btn btn-light">Light</button>
    <button type="button" class="btn btn-dark">Dark</button>
    <button type="button" class="btn btn-link">Link</button>
    <br>
    <br>
    <button type="button" class="btn btn-outline-primary">Primary</button>
    <button type="button" class="btn btn-outline-secondary">Secondary</button>
    <button type="button" class="btn btn-outline-success">Success</button>
    <button type="button" class="btn btn-outline-info">Info</button>
    <button type="button" class="btn btn-outline-warning">Warning</button>
    <button type="button" class="btn btn-outline-danger">Danger</button>
    <button type="button" class="btn btn-outline-light">Light</button>
    <button type="button" class="btn btn-outline-dark">Dark</button>
    <br>
    <br>
    <button type="button" class="btn bg-red">bg-red</button>
    <button type="button" class="btn bg-yellow">bg-yellow</button>
    <button type="button" class="btn bg-aqua">bg-aqua</button>
    <button type="button" class="btn bg-blue">bg-blue</button>
    <button type="button" class="btn bg-light-blue">bg-light-blue</button>
    <button type="button" class="btn bg-green">bg-green</button>
    <button type="button" class="btn bg-orange">bg-orange</button>
    <button type="button" class="btn bg-gray-05">bg-gray-05</button>
    <button type="button" class="btn bg-gray-10">bg-gray-10</button>
    <button type="button" class="btn bg-gray-30">bg-gray-30</button>
    <button type="button" class="btn bg-gray-70">bg-gray-70</button>
    <button type="button" class="btn bg-gray-90">bg-gray-90</button>
    <button type="button" class="btn bg-black">bg-black</button>
    <button type="button" class="btn bg-fuchsia">bg-fuchsia</button>
    <button type="button" class="btn bg-purple">bg-purple</button>
    <button type="button" class="btn bg-light-purple">bg-light-purple</button>
    <button type="button" class="btn bg-maroon">bg-maroon</button>

    <br>
    <button type="button" class="btn bg-orange icon-box icon-box-5 rounded-circle"><i class="fi-list1"></i></button>

    <hr>
    <h3>icon box</h3>
    <xmp><span class="icon-box icon-box-1 bg-blue"><i class="fi-list1"></i></span></xmp>
    <br>
    <span class="icon-box icon-box-1 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-2 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-4 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-5 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-6 bg-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-1 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-2 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-4 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-5 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-6 bg-blue rounded-circle"><i class="fi-list1"></i></span>
    <a href="#"><span class="icon-box icon-box-6 bg-blue rounded-circle"><i class="fi-list1"></i></span></a>

    <br><br>
    <span class="icon-box icon-box-3 rounded-circle border border-primary text-primary"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-secondary text-secondary"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-success text-success"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-danger text-danger"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-warning text-warning"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-info text-info"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-light text-light"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-dark text-dark"><i class="fi-list1"></i></span>
    <div class="bg-black p-1 mt-1"><span class="icon-box icon-box-3 rounded-circle border border-white text-white"><i class="fi-list1"></i></span></div>
    <br><br>
    <span class="icon-box icon-box-3 rounded-circle border border-red text-red"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-yellow text-yellow"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-aqua text-aqua"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-blue text-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-light-blue text-light-blue"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-green text-green"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-orange text-orange"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-gray-05 text-gray-05"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-gray-10 text-gray-10"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-gray-30 text-gray-30"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-gray-70 text-gray-70"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-gray-90 text-gray-90"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-black text-black"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-fuchsia text-fuchsia"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-purple text-purple"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 rounded-circle border border-light-purple text-light-purple"><i class="fi-list1"></i></span>
    <div class="bg-black p-1 mt-1"><span class="icon-box icon-box-3 rounded-circle border border-white text-white"><i class="fi-list1"></i></span></div>

    <br><br>
    <span class="img-box img-box-10 bg-gray-05 rounded-circle"><img src="assets/img/fnl-report.png"></span>
    <span class="img-box img-box-6 bg-green"><img src="assets/img/fnl-report.png"></span>
    <a href="#"><span class="img-box img-box-6 bg-gray-05 rounded-circle"><img src="assets/img/fnl-report.png"></span></a>

    <hr>
    <h3>shadow</h3>
    <xmp><span class="badge bg-aqua shadow-xs">shadow-xs</span></xmp>
    <br>
    <span class="badge bg-aqua shadow-none">shadow-none</span>
    <span class="badge bg-aqua shadow-xs">shadow-xs</span>
    <span class="badge bg-aqua shadow-sm">shadow-sm</span>
    <span class="badge bg-aqua shadow">shadow</span>

    <hr>
    <h3>rounded</h3>
    <xmp><span class="icon-box icon-box-3 bg-blue rounded-xs"><i class="fi-list1"></i></span></xmp>
    <br>
    <span class="icon-box icon-box-3 bg-blue rounded-xs"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 bg-blue rounded"><i class="fi-list1"></i></span>
    <span class="icon-box icon-box-3 bg-blue rounded-circle"><i class="fi-list1"></i></span>

    <hr>
    <h3>grid system</h3>
<xmp><section class="bg-blue-g pt-6 pb-4">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-12 pb-2">
                a
            </div>
            <div class="col-lg-6 col-md-12 pb-2">
                b
            </div>
        </div>
    </div>
</section></xmp>
    <br>
    <section class="bg-blue-g pt-6 pb-4">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-12 pb-2">
                    a
                </div>
                <div class="col-lg-6 col-md-12 pb-2">
                    b
                </div>
            </div>
        </div>
    </section>

    <hr>
    <h3>line shape</h3>
<xmp><div class="line-shape line-shape-center bg-blue"></div></xmp>
    <br>
    <div class="line-shape line-shape-center bg-blue"></div>
    <div class="line-shape bg-aqua"></div>

    <hr>
    <h3>mark, pre</h3>
    <div>this is <mark>mark tag</mark></div>
    <div><pre>pre tag</pre></div>
</div>

<?php
    include 'includes/footer3.php';
?>