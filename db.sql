-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2019 at 05:33 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sql`
--

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `oid` varchar(10) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `subject` varchar(150) NOT NULL,
  `status_id` int(11) NOT NULL,
  `last-activity` datetime NOT NULL,
  `uread` int(11) NOT NULL,
  `aread` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets`
--

INSERT INTO `tickets` (`id`, `uid`, `oid`, `cat_id`, `subject`, `status_id`, `last-activity`, `uread`, `aread`) VALUES
(1, 0, '5445456', 2, 'test1', 1, '2019-04-20 07:54:06', 1, 1),
(2, 2, '5445456', 1, 'first t', 1, '2019-04-20 08:19:49', 0, 1),
(4, 2, '5445456', 3, 'test1', 1, '2019-04-20 08:56:51', 0, 1),
(5, 2, '5445456', 3, 'test6', 1, '2019-04-20 08:57:39', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tickets_cat`
--

CREATE TABLE `tickets_cat` (
  `id` int(11) NOT NULL,
  `category` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets_cat`
--

INSERT INTO `tickets_cat` (`id`, `category`) VALUES
(1, 'Support'),
(2, 'Sales'),
(3, 'General');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_replies`
--

CREATE TABLE `tickets_replies` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `reply_by` varchar(50) NOT NULL,
  `tid` int(11) NOT NULL,
  `reply_msg` longtext NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets_replies`
--

INSERT INTO `tickets_replies` (`id`, `uid`, `reply_by`, `tid`, `reply_msg`, `time`) VALUES
(1, 2, 'tst', 2, 'dasfdasfddfsaf', '2019-04-20 08:19:50'),
(2, 2, 'tst', 3, 'dasfdasfddfsaf', '2019-04-20 08:21:25'),
(3, 2, 'tst', 4, 'fdgftgrt', '2019-04-20 08:56:51'),
(4, 2, 'tst', 5, 'fdgftgrthjkhk', '2019-04-20 08:57:39');

-- --------------------------------------------------------

--
-- Table structure for table `tickets_status`
--

CREATE TABLE `tickets_status` (
  `id` int(11) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tickets_status`
--

INSERT INTO `tickets_status` (`id`, `status`) VALUES
(1, 'Awaiting agent'),
(2, 'Responded'),
(3, 'Solved'),
(4, 'Closed');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` int(1) NOT NULL,
  `balance` int(100) NOT NULL,
  `lastactivity` int(50) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `ref_url` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `api` varchar(32) NOT NULL,
  `refu` int(11) NOT NULL,
  `exp` int(11) NOT NULL,
  `team` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `status`, `balance`, `lastactivity`, `hash`, `ref_url`, `date`, `api`, `refu`, `exp`, `team`) VALUES
(2, 'tst', 'tst@abmegypt.com', '123', 2, 0, 1555859846, '', '', '2018-09-26 00:00:00', '4f855affa142d77425c40e5a4adadd1a', 0, 1, 1),
(3, 'tst2', 'tst2@abmegypt.com', '456', 2, 1566, 1546343947, 'c22abfa379f38b5b0411bc11fa9bf92f', '', '2018-07-26 00:00:00', '1c00ca1516739c7dc5890276ba863539', 0, 1, 1),
(4, 'tst3', 'tst3@abmegypt.com', '789', 2, 0, 1546344017, '', '', '0000-00-00 00:00:00', '', 0, 0, 1),
(28729, 'tst4', 'tst4@abmegypt.com', '000', 1, 0, 1554356203, '7fe1f8abaad094e0b5cb1b01d712f708', 'http://localhost/ses_core/logout.php', '2019-04-04 05:35:30', '', 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_cat`
--
ALTER TABLE `tickets_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_replies`
--
ALTER TABLE `tickets_replies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets_status`
--
ALTER TABLE `tickets_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `status` (`status`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tickets_cat`
--
ALTER TABLE `tickets_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tickets_replies`
--
ALTER TABLE `tickets_replies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tickets_status`
--
ALTER TABLE `tickets_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28730;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
