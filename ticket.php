<?php
require './classes/db.php';
require './classes/model.php';
require './classes/tables.php';
require './classes/ticket.php';
require './classes/user.php';
require './includes/core.php';
require 'includes/header3.php';
require 'includes/nav.php';


lockRedirect('tickets.php');

$ticket = Null;
$all_replies= Null;

if($_GET['id'] && is_numeric($_GET['id']) && $_GET['id'] > 0){

	$id = model::secure($_GET['id']);
	$ticket =  tickets::find($id);
	$all_replies = tickets_replies::all_sql("where tid = $id");
}
?>

<section class="p-5">
	<div class="container">
		<div class="box box-blue p-2">

			<div class="row">
				<div class="col-lg-12">
					<h3><?= $ticket['subject'] ?></h3>
				</div>


				<div class="col-lg-12 mb-3">
					<div class="float-left">
						<p>Ticket ID:
							<span class="badge badge-warning">#<?= $ticket['id'] ?></span>
							<span>Order ID: </span><span class="badge badge-info"><?= $ticket['oid'] ?></span>
						</p>
					</div>
					<div class="float-right">
						<a   href='#' class="btn btn-outline-secondary action" url="ticket.php" data='{"action":"btn_change_uread","id":"<?= $id ?>"}'> MARK AS UNRED</a>
						

					</div>

				</div>
				<?php foreach ($all_replies as $key => $value) { ?>


					<div class="col-lg-12">
						<div class="ticket-msg mb-5">
							<div class="ticket-info clearfix">
								<h3 class="float-left f-100"><?=$value['reply_by']?></h3>
								<span class="ticket-timestamp float-right">
									<span class="badge bg-gray-10"><?=$value['time']?></span>
								</span>
							</div>
							<div class="ticket-text">
								<span class="fi-user badge-warning p-2 rounded-circle"></span>
								<?=$value['reply_msg']?>
							</div>
						</div>
						<hr />
					</div>

				<?php } ?>

				<div class="col-lg-12">
					<?php ticketClass::newTicketReply($id); ?>
				</div>
			</div> <!-- end row-->
		</div>
		<!--end BOX-->
	</div>
	<!--container -->
</section>


<?php
require 'includes/footer-menu.php';
require 'includes/footer3.php';
?>