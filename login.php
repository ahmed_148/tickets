<?php
require './classes/db.php';
require './classes/model.php';
require './classes/tables.php';
require './classes/user.php';
require './includes/core.php';
$mTtl = 'Login - SEOeStore Panel';
$mDesc = 'Login to your SEOeStore account. Manage your orders & reports.';
$mKw = 'SEOeStore, SEOeStore login, login, SEO login';
require 'includes/header3.php';

$return = 0;
if (isset($_GET['return']) AND substr( $_GET['return'], 0, 4 ) != "http") $return = $_GET['return'];

if (ucheck()){
    if ($return){
        exit("<script>document.location='./".$return."';</script>");
    }else{
        exit("<script>document.location='./index.php';</script>");
    }
}
?>

<div class="wrap-login100">

    <div class="login100-form bg-gray-05 p-5">
        <div class="pt-5">
            <div id="logo" class="text-center">
                <a href="index.php"><img src="assets/img/logo.png"></a>
            </div>

            <div class="pt-5">
                <?php userClass::showForm('login',$return)?>
            </div>

            <div class="text-center mt-5">
                Not a member? <a class="text-blue" modal='0' href="<?=$regLink?>">Register Now</a>
            </div>

        </div>
    </div>

    <div class="login100-more bg-aqua-g pt-8 text-center">
        <img class="" src="assets/img/13.png">
    </div>
</div>

<?php
require 'includes/footer3.php';
?>