<?php
require '../classes/db.php';
require '../classes/model.php';
require '../classes/tables.php';

require '../libraries/PHPMailer/src/Exception.php';
require '../libraries/PHPMailer/src/PHPMailer.php';
require '../libraries/PHPMailer/src/SMTP.php';
require '../classes/email.php';

require '../classes/user.php';

if (isset($_POST['action'])){
	$action = $_POST['action'];
}else{
	exit('error: missing action!');
}

// var_dump($_POST);

// show forms
if ($action == 'showForm-tabs'){
    $active = model::secure($_POST['active']);
    $return = model::secure($_POST['return']);
    userClass::showForm('tabs',$return,$active);
}

// login
if ($action == 'login'){
    $user = model::secure($_POST['user']);
    $password = model::secure($_POST['password']);
    $remember = 0;
    if (isset($_POST['remember'])) $remember = model::secure($_POST['remember']);
    $return = model::secure($_POST['return']);
    userClass::login($user, $password, $remember, $return);
}

// register
if ($action == 'register'){
    $uname = model::secure($_POST['uname']);
    $email = model::secure($_POST['email']);
    $password = model::secure($_POST['password']);
    $return = model::secure($_POST['return']);
    $userClass = new userClass;
    $userClass->register($uname, $email, $password, $return);
}