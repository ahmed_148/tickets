<?php
require '../classes/db.php';
require '../classes/model.php';
require '../classes/tables.php';

require '../classes/user.php';
require '../classes/ticket.php';
require '../includes/core.php';

if (isset($_POST['action'])){
	$action = $_POST['action'];
}
else{
	exit('error: missing action!');
}




if($_POST['action'] == 'show_Ticket_form'){

	ticketClass::newTicketForm();
}


if($_POST['action'] == 'btn_change_uread'){

	$tid= model::secure($_POST['id']);
	//echo $_POST['id'];
	$y = array();
	$y['uread'] = 1;
	$status_update = tickets::updateArray($y,'id',$tid);

	if($status_update){
		$json['notification'] = array('type'=>'success', 'msg'=>'Ticked Marked Successfully!');
		$json['reload'] = true;
		exit(json_encode($json, JSON_PRETTY_PRINT));
	}
}


if($_POST['action'] == 'addReply'){

	$tid= model::secure($_POST['id']);
	$status   =  model::secure($_POST['status']);
	$message  =  model::secure($_POST['message']);

	ticketClass::submitNewReply($tid,$status,$message);
}



if($_POST['action'] == 'addTicket'){

	$catID       =  model::secure($_POST['cat_id']);
	$subject     =  model::secure($_POST['subject']);
	$oid         =  model::secure($_POST['oid']);
	$message     =  model::secure($_POST['message']);

	ticketClass::submitNewTicket($catID,$subject,$oid,$message);
}


if($_POST['action'] == 'table'){
	$data = array();
	$uid = model::secure($_POST["uid"]);
	$draw = model::secure($_POST["draw"]);
    $start  = model::secure($_POST["start"]);//Paging first record indicator.
    $length = model::secure($_POST['length']);//Number of records that the ta
    $recordsTotal = $recordsFiltered = tickets::count_all();
    $param = "where `uid` = '$uid' limit $start, $length";
    $tickets = tickets::all_sql($param);

    if($tickets){
    	foreach ($tickets as $key => $ticket) {
    		$id = $ticket['id'];
    		$ticket['id'] = '<a href="./ticket.php?id='.$id.'">#'.$id.'</a>';
    		$ticket['subject'] = '<a href="./ticket.php?id='.$id.'">'.$ticket['subject'].'</a>';
    		$categoryID = $ticket['cat_id'];
    		$category = tickets_cat::where('id',$categoryID)['category'];
    		$ticket['category'] = $category;

    		switch ($ticket['status_id']) {
    			case '1':
    			$ticket['status_id'] = '<span class="badge badge-warning">Awaiting agent</span>';
    			break;
    			case '2':
    			$ticket['status_id'] = '<span class="badge badge-info">Responded</span>';
    			break;
    			case '3':
    			$ticket['status_id'] = '<span class="badge badge-success">Solved</span>';
    			break;
    			case '4':
    			$ticket['status_id'] = '<span class="badge badge-gray">Closed</span>';
    			break;
    		}

    		$recordDate = strtotime($ticket['last-activity']);
    		if (gmdate("Ymd") == date("Ymd", $recordDate)){
    			$time =  date("h:i A", $recordDate);
    			$ticket['last-activity'] = '<span class="badge badge-light" data-toggle="tooltip" data-placement="top" title="Today on '.$time.'">' . $time . '</span>';
    		}else{
    			$date = date("M", $recordDate) . " " . intval(date("d", $recordDate));
    			$fullDate = date("Y/m/d h:i A", $recordDate);
    			$ticket['last-activity'] = '<span class="badge badge-light" data-toggle="tooltip" data-placement="top" title="'.$fullDate.'">' . $date . '</span>';
    		}

    		$data[] = $ticket ; 

    	}
    }else {
    	$draw="0";
    	$recordsTotal = $recordsFiltered = "0";
    	$data =[] ;

    }

    $response = array(
    	"draw" => intval($draw),
    	"recordsTotal" => $recordsTotal,
    	"recordsFiltered" => $recordsFiltered,
    	"data" => $data
    );
    echo json_encode($response);  
}