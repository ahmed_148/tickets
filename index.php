<?php
require './classes/db.php';
require './classes/model.php';
require './classes/tables.php';
require './classes/user.php';
require './includes/core.php';
$mTtl = 'SEOeStore Panel- The #1 SEO panel on the planet';
$mKw = 'SEO, SEOeStore, SEO panel, SEOeStore panel';
$mDesc = 'SEOeStore Panel- The #1 SEO panel on the planet. Order in just few clicks, see the progress & download the reports.';

$twc = '
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@seoestore" />
<meta name="twitter:creator" content="@seoestore" />
';

$ogp = '
<meta property="og:type" content="website" />
<meta property="og:title" content="SEOeStore" />
<meta property="og:url" content="https://panel.seoestore.net/" />
<meta property="og:image" content="https://panel.seoestore.net/assets/img/logo-600-sq.png" />
<meta property="og:description" content="The #1 SEO panel on the planet... Order in just a few clicks, see the progress & download the reports." />
';

require './includes/header3.php';
// lockRedirect();
// lockModal();
// if (ucheck()){
//     echo 'found';
// }else{
//     echo 'no user';
// }
require './includes/nav.php';
?>

<section class="bg-blue-g">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 col-sm-12 text-lg-left text-center">
                <div class="mt-10" itemprop="brand" itemscope itemtype="http://schema.org/Brand">
                    <h1 class="f-300 b-8 pt-2" itemprop="name">SEOeStore</h1>
                    <h1 class="f-200 b-6" itemprop="description">The #1 SEO panel on the planet</h1>
                    <p>Order in just a few clicks, see the progress & download the reports.</p>
                </div>

<?php
    if (ucheck()) {
?>
                <div class="mt-7 mb-4">
                    <a class="btn btn-outline-light mr-2 mb-2" href="./balance.php" >1. Add Funds</a>
                    <a class="btn btn-outline-light mr-2 mb-2" href="./order.php" >2. Make Order</a>
                    <a class="btn btn-outline-light mr-2 mb-2" href="./reports.php" >3. Check Report</a>
                </div>
<?php
    }else{
?>
                <div class="mt-7 mb-4">
                    <a href="./<?=$regLink?>" ><span class="btn bg-white pt-3 pb-3 pr-5 pl-5 b-8"> GET FREE ACCESS</span></a>
                </div>
<?php
}
?>

            </div>
            <div class="col-lg-5 mt-5 pt-7 d-none d-lg-block">
                <img class="float-right" src="assets/img/13.png" alt="boost your rank">
            </div>
        </div>
    </div>
</section>


<section class="">
    <div class="container pt-8 pb-6 text-center">
        <h3 class="b-8 text-yellow">Rank your website in Google 1st page</h3>
        <p class="text-gray-70">Get Free access and start increasing your website autority, to get better rank in Google</p>

<?php
    if (ucheck()) {
?>
        <div class="mt-5">
            <a class="btn btn-outline-dark mr-2 mb-2" href="./balance.php" >1. Add Funds</a>
            <a class="btn btn-outline-dark mr-2 mb-2" href="./order.php" >2. Make Order</a>
            <a class="btn btn-outline-dark mr-2 mb-2" href="./reports.php" >3. Check Report</a>
        </div>
<?php
    }else{
?>
        <div class="mt-5">
            <a href="./<?=$regLink?>" class="btn btn-warning pt-3 pb-3 pr-5 pl-5 b-8">GET FREE ACCESS</a>
        </div>
<?php        
    }
?>

    </div>
</section>


<?php
require 'includes/footer-menu.php';
require 'includes/footer3.php';
?>