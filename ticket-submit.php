<?php
    require './classes/db.php';
    require './classes/model.php';
    require './classes/tables.php';

    require './classes/user.php';
    require './classes/ticket.php';

    require './includes/core.php';
    require 'includes/header3.php';
    require 'includes/nav.php';
?>

<section class="p-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="box box-blue p-2">
					<h3 class="f-150 mt-5 mb-3" >Submit new ticket</h3>
					<hr>
				     <?php ticketClass::newTicketForm(); ?>
				</div>
			</div>
		</div>
	</div>
</section>




<?php
require 'includes/footer-menu.php';
require 'includes/footer3.php';
?>