<?php
require 'includes/session.php';
$metaTitle = 'Ticket - SEOeStore Panel';
require 'includes/header.php';

if (!isset($_GET['id'])) {
	echo "<script type='text/javascript'> document.location = 'tickets.php'; </script>";
} elseif (!is_numeric($_GET['id'])) {
	echo "<script type='text/javascript'> document.location = 'tickets.php'; </script>";
} else {
	$tid = $_GET['id'];
    mysql_query("UPDATE `tickets` SET `uread`='0' WHERE `id`='$tid'");
}
?>

<script>
$(document).ready(function(){
    $( ".nav li a[href^='tickets.php']" ).parent().addClass( "active" );
});
</script>

<div class="end-div container-fluid">
    <div id="list-box" class="row">
        <div class="col-md-3">
            <div class="side-menu">
                <div class="side-menu-header">Ticket Menu</div>
                <ul>
                    <li>
                        <a class="web2" href="ticket-submit.php"><i class="fa fa-ticket"></i>Submit New Ticket<span>Get help/support from our team.</span></a>
                    </li>
                    <li>
                        <a class="all" href="tickets.php"><i class="fa fa-list-ul"></i>All Tickets<span>Show list of all of your tickets.<span></a>
                    </li>
                </ul>
                </div>
            </div>
            <div class="col-md-9">

<?php
$uid = $_SESSION["userid"];
$username = $_SESSION["username"];

if (isset($_POST['submit'])) {
    if (!is_numeric($_POST['status']) || !isset($_POST['message'])) {
        $general ->alert('Please fill all required fields!', 'danger');
    } else {
        $uid = $_SESSION["userid"];
        $message = mysql_real_escape_string(htmlspecialchars($_POST['message']));
        $time = gmdate('ymdHis');
        $username = $_SESSION["username"];

        $status = mysql_real_escape_string(htmlspecialchars($_POST['status']));
        if ($status == 2) {
            $statusID = 3;
        } else {
            $statusID = 1;
        }


        //insert data
        $sql = "UPDATE `tickets` SET `status_id` = '$statusID',
                `last-activity` = '$time',
                `aread` = '1'
                WHERE `id`='$tid'";

        if (mysql_query($sql, $conn)) {
            $last_id = mysql_insert_id($conn);
        } else {
            die('Could not enter data: ' . mysql_error($conn));
        }

        $sql2 = "INSERT INTO `tickets_replies` 
        (`uid`,`reply_by`,`tid`,`reply_msg`,`time`) 
        VALUES
        ('$uid','$username','$tid','$message' ,'$time')";

        //test
        $retval = mysql_query( $sql2, $conn );
        if(! $retval )
        {
          die('Could not enter data: ' . mysql_error());
        }
        $general ->alert('Your reply successfully added!', 'success');
    }
}

if (isset($_POST['uread'])) {
    $sql = "UPDATE `tickets` SET `uread` =  '1' WHERE `id` = '$tid' ";
    //test
    $retval = mysql_query( $sql, $conn );
    if(! $retval )
    {
      die('Could not enter data: ' . mysql_error());
    }
    else {
        echo "<script type='text/javascript'> document.location = 'tickets.php'; </script>";
    }
}

$sql = mysql_query("SELECT * FROM `tickets_replies` WHERE `tid` = '$tid' AND `uid`='$uid' ");
if(mysql_num_rows($sql) > 0) {
	$subject = mysql_result(mysql_query("SELECT `subject` FROM `tickets` WHERE `id`='$tid'"), 0);
    $oid = mysql_result(mysql_query("SELECT `oid` FROM `tickets` WHERE `id`='$tid'"), 0);
    $oid = str_replace(' ', '', $oid);
?>

            <div class="box box-primary">
                <div id="order-box" class="panel-body">
                    <div class="row">
                        <div class="col-md-12">  
                           <h3><?php echo $subject; ?></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="pull-left">
                                Ticket ID: <span class="label bg-yellow"><b>#<?php echo $tid; ?></b></span>
                                Order ID: <span class="label bg-aqua"><b><?php echo $oid; ?></b></span>
                            </div>

                            <div class="pull-right">
                                <form method="post">
                                    <button class="btn btn-default no-radius" name="uread"><i class="fa fa-book"></i> Mark as unread</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                    </div>

<?php
    while($row = mysql_fetch_array($sql)){
?>
	
<?php
if (strtolower($row['reply_by']) == strtolower($username)) {


?>
    <div class="ticket-msg">
      <div class="ticket-info clearfix">
        <span class="ticket-name pull-left"><?php echo $row['reply_by'];?></span>
        <span class="ticket-timestamp pull-right">
<?php
$recordDate = strtotime($row['time']);
if (gmdate("Ymd") == date("Ymd", $recordDate)){
    $time =  date("h:i A", $recordDate);
    $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
}
else {
    $date = date("M", $recordDate) . " " . intval(date("d", $recordDate));
    $fullDate = date("Y/m/d h:i A", $recordDate);
    $general ->toolTipClass($date, $fullDate, 'label label-gray');
}
?>
	</span>
      </div><!-- /.ticket-info -->
      <i class="fa fa-user fa-fw thumb text-gray bg-yellow"></i><!-- /.ticket-img -->
      <div class="ticket-text">
        <?php echo nl2br($row['reply_msg']);?>
      </div><!-- /.ticket-text -->
    </div>
<?php
} else {
?>
	<div class="ticket-msg right">
      <div class="ticket-info clearfix">
        <span class="ticket-name pull-right"><?php echo $row['reply_by'];?></span>
        <span class="ticket-timestamp pull-left">
<?php
$recordDate = strtotime($row['time']);
if (gmdate("Ymd") == date("Ymd", $recordDate)){
    $time =  date("h:i A", $recordDate);
    $general ->toolTipClass($time , "Today on " . $time, 'label label-gray');
}
else {
    $date = date("M", $recordDate) . " " . intval(date("d", $recordDate));
    $fullDate = date("Y/m/d h:i A", $recordDate);
    $general ->toolTipClass($date, $fullDate, 'label label-gray');
}
?>
        </span>
      </div><!-- /.ticket-info -->
      <i class="fa fa-support thumb bg-blue"></i><!-- /.ticket-img -->
      <div class="ticket-text">
        <?php echo nl2br($row['reply_msg']);?>
      </div><!-- /.ticket-text -->
    </div>

<?php
}
?>

<?php
    }
}
else {
    echo "<script type='text/javascript'> document.location = 'tickets.php'; </script>";
}

?>

<br><hr>

        			<form method="post" action="">
        				<div class="form-group">
        					<label for="status">Status:</label>
        		            <select class="form-control" id="status" name="status" required>
                                <option value="1">Open</option>
        		                <option value="2">Solved</option>
        		            </select>
        				</div>

        				<div class="form-group">
        					<label for="message">Message:</label>
        					<textarea class="form-control"  rows="5" id="message" name="message" placeholder="Message..." required></textarea>
        				</div>
        				<input class="btn btn-primary" type="submit" name="submit" value="Submit">
        			</form>

                </div>
            </div>
		</div>
	</div>
</div>

<?php
include 'includes/footer.php';
?>