<?php
require './classes/db.php';
require './classes/model.php';
require './classes/tables.php';

require './classes/user.php';
require './classes/ticket.php';

require './includes/core.php';
require 'includes/header3.php';
require 'includes/nav.php';

//redirect to the same url
lockRedirect();
?>

<!-- datatable -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<section class="p-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="box box-blue p-2">
                    <h3 class="mb-5">Tickets <a onclick="modalShow('ticket.php',{action:'show_Ticket_form'})"
                        class="btn btn-primary text-white">Submit New Ticket</a></h3>

                        <?php ticketClass::tableTicket(); ?>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    require 'includes/footer-menu.php';
    require 'includes/footer3.php';
    ?>