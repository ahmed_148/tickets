<?php
class admins extends model{
	protected $table = '`admins`';
}
class faqs extends model{
	protected $table = '`faqs`';
}
class more_detalis extends model{
	protected $table = '`more_detalis`';
}
class affiliate_balance extends model{
	protected $table = '`affiliate_balance`';
}
class affiliate_earnings extends model{
	protected $table = '`affiliate_earnings`';
}
class affiliate_options extends model{
	protected $table = '`affiliate_options`';
}
class affiliate_withdraw extends model{
	protected $table = '`affiliate_withdraw`';
}
class affiliate_withdraw_methods extends model{
	protected $table = '`affiliate_withdraw_methods`';
}
class affiliate_withdraw_status extends model{
	protected $table = '`affiliate_withdraw_status`';
}
class billing_info extends model{
	protected $table = '`billing_info`';
}
class btc_addresses extends model{
	protected $table = '`btc_addresses`';
}
class btc_txn extends model{
	protected $table = '`btc_txn`';
}
class user_status extends model{
	protected $table = '`user_status`';
}
class users_details extends model{
	protected $table = '`users_details`';
}
class users extends model{
	protected $table = '`users`';
}
class tw_status extends model{
	protected $table = '`tw_status`';
}
class tw_blacklist extends model{
	protected $table = '`tw_blacklist`';
}
class tickets_status extends model{
	protected $table = '`tickets_status`';
}
class tickets_replies extends model{
	protected $table = '`tickets_replies`';
}
class tickets_cat extends model{
	protected $table = '`tickets_cat`';
}
class tickets extends model{
	protected $table = '`tickets`';
}
class status extends model{
	protected $table = '`status`';
}
class sps extends model{
	protected $table = '`sps`';
}
class ser_orders extends model{
	protected $table = '`ser_orders`';
}
class ser_get extends model{
	protected $table = '`ser_get`';
}
class ser_engines extends model{
	protected $table = '`ser_engines`';
}
class rtcl_cats_dtls extends model{
	protected $table = '`rtcl_cats_dtls`';
}
class promo_msg extends model{
	protected $table = '`promo_msg`';
}
class popup extends model{
	protected $table = '`popup`';
}
class paypal extends model{
	protected $table = '`paypal`';
}
class old_emails extends model{
	protected $table = '`old_emails`';
}
class offers_used extends model{
	protected $table = '`offers_used`';
}
class offers_type extends model{
	protected $table = '`offers_type`';
}
class offers extends model{
	protected $table = '`offers`';
}
class managers extends model{
	protected $table = '`managers`';
}
class i3 extends model{
	protected $table = '`i3`';
}
class i2 extends model{
	protected $table = '`i2`';
}
class i1 extends model{
	protected $table = '`i1`';
}
class free_balance extends model{
	protected $table = '`free_balance`';
}
class fnl_types extends model{
	protected $table = '`fnl_types`';
}
class fnl_status extends model{
	protected $table = '`fnl_status`';
}
class fnl_offer extends model{
	protected $table = '`fnl_offer`';
}
class fnl_leads extends model{
	protected $table = '`fnl_leads`';
}
class fnl_extra extends model{
	protected $table = '`fnl_extra`';
}
class fnl_core extends model{
	protected $table = '`fnl_core`';
}
class files extends model{
	protected $table = '`files`';
}
class fb_leads extends model{
	protected $table = '`fb_leads`';
}
class extras extends model{
	protected $table = '`extras`';
}
class deals extends model{
	protected $table = '`deals`';
}
class custom_campaign extends model{
	protected $table = '`custom_campaign`';
}
class coupon_used extends model{
	protected $table = '`coupon_used`';
}
class coupon extends model{
	protected $table = '`coupon`';
}
class cc_cat extends model{
	protected $table = '`cc_cat`';
}
class cc_artcl extends model{
	protected $table = '`cc_artcl`';
}
class campaigns extends model{
	protected $table = '`campaigns`';
}
class btc_wallets extends model{
	protected $table = '`btc_wallets`';
}
class coupons_tmp extends model{
	protected $table = '`coupons_tmp`';
}
class coupons_tmp1 extends model{
	protected $table = '`coupons_tmp1`';
}
class love_seo extends model{
	protected $table = '`love_seo`';
}
class coupons_tmp4 extends model{
	protected $table = '`coupons_tmp4`';
}
class slider extends model{
	protected $table = '`slider`';
}
class tap_txn extends model{
	protected $table = '`tap_txn`';
}
class boss_id_1 extends model{
	protected $table = '`boss_id_1`';
}
class boss_id_2 extends model{
	protected $table = '`boss_id_2`';
}
