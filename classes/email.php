<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class emailClass{

    // Start mailer Method
    private static function mailer($to, $subject, $html, $altbody){
        $a = new static;
        $mail = new PHPMailer;
        //Server settings
        $mail->SMTPDebug = 0;
        $mail->isSMTP();
        $mail->Host = 'seoestore.net';
        $mail->SMTPAuth = true; 
        $mail->Username = 'admin@seoestore.net';
        $mail->Password = '29LHE4SmGvm5';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = '465';  
        $mail->isHTML(true);

        $mail->setFrom('admin@seoestore.net', 'SEOeStore');

        $mail->addAddress($to);
        $mail->Subject = $subject;
        $mail->Body = $html;
        $mail->AltBody = $altbody;

        if (!$mail->send()) {
            //The reason for failing to send will be in $mail->ErrorInfo
            return False;
        } else {
            return True;
        }
    }
    // End mailer Method

    // Start verify Method
    public static function verify($uid){
        $userData = users::find($uid);
        $to = $userData['email'];

        $subject = file_get_contents("https://panel.seoestore.net/email_tpl/verify-subject.txt");

        $html = file_get_contents("https://panel.seoestore.net/email_tpl/verify.html");
        $html = str_replace("{{username}}", $userData['username'], $html);
        $html = str_replace("{{email}}", $userData['email'], $html);
        $html = str_replace("{{hash}}", $userData['hash'], $html);
        
        $altbody = file_get_contents("https://panel.seoestore.net/email_tpl/verify.txt");
        $altbody = str_replace("{{username}}", $userData['username'], $altbody);
        $altbody = str_replace("{{email}}", $userData['email'], $altbody);
        $altbody = str_replace("{{hash}}", $userData['hash'], $altbody);

        // echo $subject;
        // echo $html;
        // echo'<pre>';
        // echo $altbody;
        // echo'</pre>';

        return(self::mailer($to, $subject, $html, $altbody));
    }
    // END verify Method


    // Start register Method
    public static function register($uid){
        $userData = users::find($uid);
        $to = $userData['email'];

        $subject = file_get_contents("https://panel.seoestore.net/email_tpl/register-subject.txt");

        $html = file_get_contents("https://panel.seoestore.net/email_tpl/register.html");
        $html = str_replace("{{username}}", $userData['username'], $html);
        $html = str_replace("{{email}}", $userData['email'], $html);
        $html = str_replace("{{hash}}", $userData['hash'], $html);
        
        $altbody = file_get_contents("https://panel.seoestore.net/email_tpl/register.txt");
        $altbody = str_replace("{{username}}", $userData['username'], $altbody);
        $altbody = str_replace("{{email}}", $userData['email'], $altbody);
        $altbody = str_replace("{{hash}}", $userData['hash'], $altbody);

        // echo $subject;
        // echo $html;
        // echo'<pre>';
        // echo $altbody;
        // echo'</pre>';

        return(self::mailer($to, $subject, $html, $altbody));
    }
    // END register Method
    
    // Start coupon Method
    public static function coupon($uid){
        $userData = users::find($uid);
        $to = $userData['email'];

        $subject = file_get_contents("https://panel.seoestore.net/email_tpl/coupon-subject.txt");

        $html = file_get_contents("https://panel.seoestore.net/email_tpl/coupon.html");
        $html = str_replace("{{username}}", $userData['username'], $html);
        $html = str_replace("{{email}}", $userData['email'], $html);
        $html = str_replace("{{hash}}", $userData['hash'], $html);
        
        $altbody = file_get_contents("https://panel.seoestore.net/email_tpl/coupon.txt");
        $altbody = str_replace("{{username}}", $userData['username'], $altbody);
        $altbody = str_replace("{{email}}", $userData['email'], $altbody);
        $altbody = str_replace("{{hash}}", $userData['hash'], $altbody);

        // echo $subject;
        // echo $html;
        // echo'<pre>';
        // echo $altbody;
        // echo'</pre>';

        return(self::mailer($to, $subject, $html, $altbody));
    }
    // END coupon Method

    // Start contact Method
    public static function contact($uid,$name,$email,$subject,$message){
        if ($uid != 0) {
            $userData = users::find($uid);
            $uid = $userData['username'];
        }
        $to = 'admin@seoestore.net';

        $subject = file_get_contents("https://panel.seoestore.net/email_tpl/contact-subject.txt");

        $msgHtml = str_replace('\r\n',"<br />",$message);
        $html = file_get_contents("https://panel.seoestore.net/email_tpl/contact.html");
        $html = str_replace("{{username}}", $uid, $html);
        $html = str_replace("{{name}}", $name, $html);
        $html = str_replace("{{email}}", $email, $html);
        $html = str_replace("{{subject}}", $subject, $html);
        $html = str_replace("{{message}}", $msgHtml, $html);
        
        $altbody = file_get_contents("https://panel.seoestore.net/email_tpl/contact.txt");
        $html = str_replace("{{username}}", $uid, $html);
        $html = str_replace("{{name}}", $name, $html);
        $html = str_replace("{{email}}", $email, $html);
        $html = str_replace("{{subject}}", $subject, $html);
        $html = str_replace("{{message}}", $message, $html);

        // echo $subject;
        // echo $html;
        // echo'<pre>';
        // echo $altbody;
        // echo'</pre>';

        return(self::mailer($to, $subject, $html, $altbody));
    }
    // END contact Method

}
?>