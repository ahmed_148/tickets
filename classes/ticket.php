<?php

class ticketClass{

	public static function newTicketForm(){ ?>

		<form method="post" onsubmit="submitForm(this,'ticket.php')" prevent-default>
			<input type="hidden" name="action" value="addTicket">
			<div class="form-group">
				<label for="formGroupExampleInput" class="b-6">Category:<span class="text-red">*</span></label>
				<select class="custom-select" name='cat_id' placeholder="Select Category..." required>
					<option><?= tickets_cat::option();?></option>
				</select>
			</div>
			<div class="form-group">
				<label for="formGroupExampleInput2" class="b-6">Subject:<span class="text-red">*</span></label>
				<input type="text" class="form-control" id="formGroupExampleInput2" name="subject" placeholder="Subject..."
				required>
			</div>
			<div class="form-group">
				<label for="formGroupExampleInput2" class="b-6">Order ID:</label>
				<input type="text" class="form-control" id="formGroupExampleInput2" name="oid">
			</div>
			<div class="form-group">
				<label for="exampleFormControlTextarea1" class="b-6">Message:<span class="text-red">*</span></label>
				<textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="4" placeholder="Message..."
				required></textarea>
			</div>
			<button type="submit" name="submit" class="btn btn-primary"></span>Submit New Ticket</button>
			<!-- <a class="btn btn-outline-dark" href="./tickets.php"><span class="fi-redo"></span> Back to Tickets</a> -->
			<p class="f-80 mt-3">Note: all fields with <span class="text-red">*</span> are required!</p>
		</form>
		<?php
	}

	public static function submitNewTicket($catID,$subject,$oid,$message){

		if(!isset($_SESSION['uname'])){
			$json['notification'] = array('type'=>'warning', 'msg'=>'Please login');
			$json['redirect'] = 'login.php?return=./ticket-submit.php';
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		$uname = $_SESSION['uname'];
		$uid = users::where('username',$uname)['id'];

        //validation 
		if (empty($catID)) {
			$json['notification'] = array('type'=>'danger', 'msg'=>'Category is required!');
			exit(json_encode($json, JSON_PRETTY_PRINT));
		} 

		if (empty($subject)) {
			$json['notification'] = array('type'=>'danger', 'msg'=>'subject is required!');
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}

		if (empty($message)) {
			$json['notification'] = array('type'=>'danger', 'msg'=>'message is required!');
			exit(json_encode($json, JSON_PRETTY_PRINT));
		} 

		$x = array();
		$x['uid'] = $uid;
		$x['oid'] = $oid;
		$x['cat_id'] = $catID;
		$x['subject'] = $subject;
		$x['status_id'] = 1;
		$x['last-activity'] = gmdate('ymdHis');
		$x['uread']=0;
		$x['aread'] = 1;
		$ticketsID = tickets::saveArray($x);

		$x = array();
		$x['uid'] = $uid;
		$x['reply_by'] = $uname;
		$x['tid'] = $ticketsID;
		$x['reply_msg'] = $message;
		$x['time'] = gmdate('ymdHis');
		$save = tickets_replies::saveArray($x);

		if ($save) {
			$json['notification'] = array('type'=>'success', 'msg'=>'Ticket Successfully Submitted!');
			$json['reload'] = true;
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}

		$json['notification'] = array('type'=>'danger', 'msg'=>'error while saving ticket!');
		exit(json_encode($json, JSON_PRETTY_PRINT));
	}

	public static function newTicketReply($id){
		?>
		<form method="POST" onsubmit="submitForm(this, 'ticket.php')" prevent-default>

			<input type="hidden" name="action" value="addReply">
			<input type="hidden" name="id" value="<?=$id?>">

			<div class="form-group">
				<label for="formGroupExampleInput" class="b-6">Status:</label>
				<select name="status" class="custom-select">
					<?= tickets_status::option_status();?>
				</select>
			</div>
			<div class="form-group">
				<label for="exampleFormControlTextarea1"  class="b-6">Message:</label>
				<textarea class="form-control" id="exampleFormControlTextarea1" name="message" rows="4" placeholder="Message..."></textarea>
			</div>
			<button type="submit" name="submit" class="btn bg-blue">SUBMIT</button>
		</form>
		<?php
	}

	public static function	submitNewReply($tid ,$status,$message){

		if(!isset($_SESSION['uname'])){
			$json['notification'] = array('type'=>'warning', 'msg'=>'Please login');
			$json['redirect'] = 'login.php?return=./ticket-submit.php';
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		$uname = $_SESSION['uname'];
		$uid = users::where('username',$uname)['id'];



		if (empty($status)) {
			$json['notification'] = array('type'=>'danger', 'msg'=>'Status is required!');
			exit(json_encode($json, JSON_PRETTY_PRINT));
		} 

		if (empty($message)) {
			$json['notification'] = array('type'=>'danger', 'msg'=>'Message is required!');
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		
		$x = array();
		$x['tid']= $tid ;
		$x['uid'] = $uid;
		$x['reply_by'] = $uname;
		$x['reply_msg'] = $message;
		$x['time'] = gmdate('ymdHis');
		$save = tickets_replies::saveArray($x);


		$y = array();
		$y['status_id'] = $status;
		$status_update = tickets::updateArray($y,'id',$tid);

		if($save && $status_update){
			$json['notification'] = array('type'=>'success', 'msg'=>'Reply Successfully Submitted!');
			$json['reload'] = true;
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}
		$json['notification'] = array('type'=>'danger', 'msg'=>'error while saving reply to your ticket!');
		exit(json_encode($json, JSON_PRETTY_PRINT));
	}

	public static function tableTicket(){
		
		if(!isset($_SESSION['uname'])){
			$json['notification'] = array('type'=>'warning', 'msg'=>'Please login');
			$json['redirect'] = 'login.php?return=./ticket-submit.php';
			exit(json_encode($json, JSON_PRETTY_PRINT));
		}

		$uname = $_SESSION['uname'];
		$uid = users::where('username',$uname)['id'];
		?>

		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Ticket ID</th>
					<th>Subject</th>
					<th>Category</th>
					<th>Order ID</th>
					<th>Status</th>
					<th>Last update</th>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>

		<script>
			$(function() {
				$('.table').DataTable({
					"columns": [{
						"data": "id"
					},
					{
						"data": "subject"
					},
					{
						"data": "category"
					},
					{
						"data": "oid"
					},
					{
						"data": "status_id"
					},
					{
						"data": "last-activity"
					}
					],
					"ordering": false,
					"lengthMenu": [10, 25, 50, 100],
					"processing": true,
					"serverSide": true,
					"ajax": {
						url: 'ajx/ticket.php',
						data: {
							action: 'table',
							uid: <?= $uid ?>
						},
						type: 'POST'
					}
				});
			});
		</script>

		<?php
	}

}

?>