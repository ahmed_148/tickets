<?php
if(!isset($_SESSION)) session_start();
class userClass{
	// Start SHOW FORM Method
	public static function showForm($type = 'tabs', $return = 0, $activeTab = 0, $uname = 0, $email = 0){
        if (isset($_SESSION['uname'])) {
            ?>
            <div class="text-aqua b-6 text-center">Already logged in</div>
            <script>location.reload();</script>
            <?php
            exit;
        }
        if ($type == 'login'){
            self::loginForm($uname, $return);
        }
        if ($type == 'register'){
            self::regForm($email, $return);
        }
        if ($type == 'tabs'){
            self::tabsForm($activeTab, $return, $uname, $email);
        }
	}
    // End SHOW FORM Method

    // Start LoginForm Method
    private static function loginForm($uname = 0,  $return = 0){
?>
<h5>Login to your account.</h5><div class="line-shape bg-blue mb-4"></div>
<form method="POST" action="./login.php" onsubmit="submitForm(this, 'user.php')" prevent-default>
<input name="action" type="hidden" value="login"/>
<input name="return" type="hidden" value="<?=$return?>"/>
<?php
    if ($uname){
        echo '<h4 class="text-yellow text-uper margin-top-40">Hi, <span id="uname">'.$uname.'</span></h4>';
        echo '<input type="hidden" name="user" value="<?=$username?>" required/>';
    }else{
        echo '<div class="form-group">
          <label for="username">Username or Email:</label><input class="form-control" id="username" placeholder="Username or Email" type="text" name="user" required/>
        </div>';
    }
?>
<div class="form-group">
        <label for="password">Password:</label><input class="form-control" id="password" placeholder="Password" type="password" name="password" required/>
    </div>
    <div class="checkbox small">
        <label class="padding-left-0"><input id="remember" type="checkbox" name="remember" value="1" class="check-record minimal"> Remember me for <u>30 days</u></label>
    </div>
    <div class="form-group">
        <input type="submit" name="submit" class="btn btn-primary" value="login" >
    </div>

    <div class="form-group text-right small">
        <a target="blank" href="reset.php?username">Forget username</a>
         - 
        <a target="blank" href="reset.php?password">Forget password</a>
    </div> 
</form>
<script>
    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass: 'iradio_minimal-blue'
    });
</script>
<?php
    }
    // END LoginForm Method

    // Start regForm Method
    private static function regForm($email = 0,  $return = 0){
        if ($email === 0) $email = '';
?>
<h5>Get instant access.</h5><div class="line-shape bg-blue mb-4"></div>
<form method="POST" action="./register.php" onsubmit="submitForm(this, 'user.php')" prevent-default>
    <input name="action" type="hidden" value="register"/>
    <input name="return" type="hidden" value="<?=$return?>"/>
    <div class="form-group">
        <label for="reg-username">User Name:</label><input class="form-control" id="reg-username" placeholder="User Name" type="text" name="uname" required/>
    </div>
    <div class="form-group">
        <label for="reg-email">Email:</label><input class="form-control" id="reg-email" placeholder="Email" type="email" name="email" value="<?=$email?>" required/>
    </div>
    <div class="form-group">
        <label for="reg-password">Password:</label><input class="form-control" id="reg-password" placeholder="Password" type="password" name="password" required/>
    </div>
    <input type="submit" name="submit" class="btn btn-primary" value="register" >
</form>
<?php
    }
    // END regForm Method

    // Start tabsForm Method
    private static function tabsForm($activeTab = 0, $return = 0, $uname = 0, $email = 0){
        $loginStatus = '';
        $loginShowStatus = '';
        $regStatus = 'active';
        $regShowStatus = 'show active';
        if ($activeTab === 'login'){
            $loginStatus = 'active';
            $loginShowStatus = 'show active';
            $regStatus = '';
            $regShowStatus = '';
        }
?>
<ul class="nav nav-tabs" role="tablist">
  <li class="nav-item">
    <a class="nav-link <?=$regStatus?>" id="register-tab" data-toggle="tab" href="#register" role="tab" aria-controls="register" aria-selected="true">Register</a>
  </li>
  <li class="nav-item">
    <a class="nav-link <?=$loginStatus?>" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="false">Login</a>
  </li>
</ul>
<div class="tab-content mt-3">
  <div class="tab-pane fade <?=$regShowStatus?>" id="register" role="tabpanel" aria-labelledby="register-tab">
    <?php self::regForm($email, 0, $return)?>
  </div>
  <div class="tab-pane fade <?=$loginShowStatus?>" id="login" role="tabpanel" aria-labelledby="login-tab">
    <?php self::loginForm($uname, 0, $return)?>
  </div>
</div>

<?php
    }
    // END tabsForm Method

    // Start Login Method
    public static function login($user, $password, $remember, $return){
        // if session
        if(isset($_SESSION['uname'])) {
            $json['notification'] = array('type'=>'info', 'msg'=>'Already logged in');
            $json['reload'] = true;
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        // get record
        if (strpos($user, '@') !== false) {
            $userRecord = users::where('email',$user);
        }else {
            $userRecord = users::where('username',$user);
        }
        // if invalid usernmae or email
        if(!$userRecord){
            $json['notification'] = array('type'=>'danger', 'msg'=>'Invalid username or email');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        // check if suspended
        if($userRecord['status']==3){
            $json['notification'] = array('type'=>'danger', 'msg'=>'account suspended!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        // check password
        $recordPasword = $userRecord['password'];
        if($recordPasword !== $password){
            $json['notification'] = array('type'=>'danger', 'msg'=>'Invalid password!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        //// login success
        $uname = $userRecord['username'];
        $_SESSION['uname']= $uname;
        if ($remember==1){
            setcookie('username', $uname, time() + (86400 * 30), "/"); // 86400 = 1 day
        }
        $json['notification'] = array('type'=>'success', 'msg'=>'Login success!');
        if ($return){
            $json['redirect'] = $return;
        }else{
            $json['reload'] = true;
        }
        exit(json_encode($json, JSON_PRETTY_PRINT));
    }
    // End Login Method

    // Start register Method
    public static function register($uname, $email, $password, $return){
        // check conditions
        if (strlen($uname) < 4) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Username must be at least 4 characters!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        if (!preg_match("/^[0-9A-Za-z._-]*$/",$uname)) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter a valid username, available characters: a-z A-Z 0-9 . _ and -');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter a valid email');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        if ($password==''){
            $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter a password');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        $checkUname = users::where('username',$uname);
        if ($checkUname){
            $json['notification'] = array('type'=>'danger', 'msg'=>'Username already exists, please choose another one!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        $checkEmail = users::where('email',$email);
        if ($checkEmail){
            $json['notification'] = array('type'=>'danger', 'msg'=>'Email already exists!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }

        // prepare fields values
        if(isset($_COOKIE['refId']) AND is_numeric($_COOKIE['refId'])){
            $refu = $_COOKIE['refId'];
        }else{
            $refu = 0;
        }
        $hash = md5(rand(0,1000));
        if (isset($_SESSION['refURL'])) {
            $refURL = $_SESSION['refURL'];
        } else {
            $refURL = "NA";
        }
        // insert user record
        $x = array();
        $x['username'] = $uname;
        $x['email'] = $email;
        $x['password'] = $password;
        $x['status'] = 1;
        $x['hash'] = $hash;
        $x['ref_url'] = $refURL;
        $x['refu'] = $refu;
        $x['date'] = gmdate('ymdHis');
        $uid = users::saveArray($x);

        if(!$uid){
            $json['notification'] = array('type'=>'danger', 'msg'=>'An unknown error occured. Please reload and try again!');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
        $_SESSION['uname']= $uname;

        // // old email
        // $to = $email;
        // $subject = 'Activate your new SEOeStore account';
        // $message = '
         
        // Thanks for signing up!
        // Your account has been created, you can login with the following credentials.
         
        // ------------------------
        // Username: '.$uname.'
        // Password: '.$password.'
        // ------------------------

        // Please click this link to activate your account:
        // https://panel.seoestore.net/verify.php?email='.$email.'&code='.$hash.'
         
        // ';
                             
        // $headers = 'From: "SEOeStore" <admin@seoestore.net>' . "\r\n";
        // mail($to, $subject, $message, $headers);

        // new email
        emailClass::register($uid);

        
        $json['notification'] = array('type'=>'success', 'msg'=>'Account successfully created!');
        $json['modal'] = '<div class="mt-3 text-center">';
        $json['modal'] .= '<div class="text-green b-6">Account successfully created!</div>';
        $json['modal'] .= '<div class="text-orange">Check your email to activate your account!</div>';
        $link = '';
        if ($return) $link = $return;
        $json['modal'] .= '<div class="mt-4"><a href="'.$link.'" class="btn btn-primary">Continue...</a></div>';
        $json['modal'] .= '</div">';
        $json['modalLock'] = true;
        exit(json_encode($json, JSON_PRETTY_PRINT));
    }
    // End register Method

    


     public static function updateemail($email){
        $useremail = users::where('email' , $email)['email'];
          $iddata= users::where('id' , 2); 
          if (empty($email)) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter  email');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }


         elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter a valid email');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }

        elseif ($email==$useremail) {
            $json['notification'] = array('type'=>'danger', 'msg'=>'Email already exists');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
      else{
         $x = new users;
         $x->id = $iddata['id'];
         $x->email = $email;
         $x->update();
        $json['notification'] = array('type'=>'success', 'msg'=>'Email Updated');
            exit(json_encode($json, JSON_PRETTY_PRINT));
        }
                


     }




    public static function emailform(){
        $user = users::find(2);

 ?>  
                 
                    <div class="line-shape bg-blue mb-4"></div>
                    <form id="emailform" method="post" onsubmit="submitForm(this, 'profile.php')" prevent-default>
                        <div class="form-group">
                            <label>Email:</label>
                            <input type="email" name="email" value="<?=$user['email']?>" class="form-control">
                            <input type="hidden" name="action"  value="updateemail" class="form-control">
                        </div>
                        <button type="submit" class="btn btn-primary">ADD EMAIL</button>
                    </form>


<?php  }
public static function userdiv($userid){
  $user = users::find($userid);

 ?>  <!-- 
                  <span class="img-box img-box-10 bg-gray-05 rounded-circle mt-3">
                    <img src="assets/img/fnl-report.png">
                </span> -->
                <h2 class="m-3"><?=$user['username']?></h2>
                <p class="f-150">Your Balance <span>$<?=$user['balance']/100?></span></p>




<?php  }

public static function passwordform(){
    ?>

           <form id="passform" method="post" onsubmit="submitForm(this, 'profile.php')" prevent-default>
                        <div class="form-group">
                            <input type="hidden" name="action"  value="updatepassword" class="form-control">
                            <label>Current Password:</label>
                            <input type="Password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <label>New Password:</label>
                            <input type="Password" class="form-control" name="newpass">
                        </div>
                        <div class="form-group">
                            <label>Confirm New Password:</label>
                            <input type="Password" class="form-control" name="confirmpass">
                             
                        </div>
                        <button type="submit" class="btn btn-primary">UPDATE PASSWORD</button>

                </form>
              



<?php
}


public static function updatepassword($oldpassword,$newpassword,$confirmpassword){
    $dbpassword =users::where("id",2);

            if (empty($oldpassword)||empty($newpassword)||empty($confirmpassword)) {
                $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter all fields');
                        exit(json_encode($json, JSON_PRETTY_PRINT));
                    }

            elseif($dbpassword['password'] != $oldpassword){
                   $json['notification'] = array('type'=>'danger', 'msg'=>'Password is incorrect');
                    exit(json_encode($json, JSON_PRETTY_PRINT));
               }
            elseif($newpassword !=$confirmpassword){
                       $json['notification'] = array('type'=>'danger', 'msg'=>'Password is not identical');
                        exit(json_encode($json, JSON_PRETTY_PRINT));
                }
    
                        
                              $user= new users;
                              $user->id = 2;
                              $user->password=$newpassword;
                              $user->update();
                              $json['notification'] = array('type'=>'success', 'msg'=>'Password Updated');
                                 exit(json_encode($json, JSON_PRETTY_PRINT));

                            

                              } 
            

        



public static function billingform(){ 

         $userh = billing_info::where("uid",2); 

    ?>
                <div  id="billingform1-result"></div>
                    <form id="billingform1" onsubmit="submitForm(this, 'profile.php')" method="post" prevent-default>
                        <input type="hidden" name="action"  value="savebillingdata" class="form-control">

                        <div class="form-group">
                            <label>Full Name:</label>
                            <input type="text" class="form-control" name="name" value="<?=$userh['name']?>" >
                        </div>
                        <div class="form-group">
                            <label>Address:</label>
                            <input type="text" class="form-control" name="Address1" value="<?=$userh['address']?>">
                        </div>
                        <div class="form-group">
                            <label>Address line2: <span class="f-80 text-gray-30">Optional</span></label>
                            <input type="text" class="form-control"  name="Address2" value="<?=$userh['address2']?>">
                              
                        </div>
                        <button type="submit" class="btn btn-primary">UPDATE BILLING INFO</button>
                        <p class="mt-3">Download your invoice(s) from <a href="">payments page</a></p>
                    </form>


<?php
} 
    public static function savebillingdata($name,$Address1,$Address2){
        /* $user = billing_info::where("uid",$userid)*/;
                $id =billing_info::where("uid",2)['id'];
                $data=billing_info::where("uid",2);

                 

                  if($name==$data['name'] && $Address1==$data['address'] && $Address2==$data['address2'] ){
                            $json['notification'] = array('type'=>'danger', 'msg'=>'Data is already exsist');
                                 exit(json_encode($json, JSON_PRETTY_PRINT));
                            }

                  elseif(empty($name)||empty($Address1)||empty($address2)) {
                        $json['notification'] = array('type'=>'danger', 'msg'=>'Empty Field');
                                    exit(json_encode($json, JSON_PRETTY_PRINT));
                                }
                          

                          $userinfo= new billing_info;
                                    $userinfo->id = $id;
                                    $userinfo->name=$name;
                                    $userinfo->address=$Address1;
                                    $userinfo->address2=$Address2;
                                    $userinfo->update();
                                
                              $json['notification'] = array('type'=>'success', 'msg'=>'Billing info updated');
                                 exit(json_encode($json, JSON_PRETTY_PRINT));

                   }




public static function userinfoform(){
  $userdet = users_details::where('uid',2);

    ?>
    <div class="line-shape bg-blue mb-4" id="userform-result"></div>
                     <form id="userform" onsubmit="submitForm(this, 'profile.php')" prevent-default method="post">
                      <div class="form-group">
                        <label>full name</label>
                        <input type="text" class="form-control" name="username"value="<?= $userdet['full_name']?>">
                      </div>
                      
                      <div class="form-group">
                        <label for="exampleFormControlSelect1">Country:</label>
                        <select class="form-control" id="exampleFormControlSelect1" name="Country">
                          <option>1</option>
                          <option>2</option>
                          <option>3</option>
                          <option>4</option>
                          <option>5</option>
                        </select>
                         <input type="hidden" name="action" value="userdata" class="form-control">
                      </div>
                      <button type="submit" class="btn btn-primary">UPDATE DETAILS</button>
                    </form>
    



<?php 
}


        public static function userdata($name,$country){
               $userdet = users_details::where('uid',2);
              

               if($name==$userdet['full_name'] && $country==$userdet['country'] ){
                            $json['notification'] = array('type'=>'danger', 'msg'=>'Data is already exsist');
                                 exit(json_encode($json, JSON_PRETTY_PRINT));
                            }

               elseif(empty($name)||empty($country)) {
                        $json['notification'] = array('type'=>'danger', 'msg'=>'Please enter All Fields');
                         exit(json_encode($json, JSON_PRETTY_PRINT));
                    }

                        $x = new users_details;
                        $x->id = $userdet['id'];
                        $x->full_name = $name;
                        $x->country = $country;
                        $TEST=$x->update();

                        if($TEST==1){
                            $json['notification'] = array('type'=>'success', 'msg'=>'User info updated');
                                exit(json_encode($json, JSON_PRETTY_PRINT));
                             }
                        

         }
}

?>