<?php
require 'includes/session.php';
$metaTitle = 'Submit ticket - SEOeStore Panel';
require 'includes/header.php';
?>

<script>
$(document).ready(function(){
    $( ".nav li a[href^='tickets.php']" ).parent().addClass( "active" );
});
</script>

<div class="end-div container-fluid">
    <div id="list-box" class="row">
        <div class="col-md-3">
            <div class="side-menu">
                <div class="side-menu-header">Ticket Menu</div>
                <ul>
                    <li class="active">
                        <a class="web2" href="ticket-submit.php"><i class="fa fa-ticket"></i>Submit New Ticket<span>Get help/support from our team.</span></a>
                    </li>
                    <li>
                        <a class="all" href="tickets.php"><i class="fa fa-list-ul"></i>All Tickets<span>Show list of all of your tickets.<span></a>
                    </li>
                </ul>
            	</div>
            </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div id="order-box" class="panel-body">
				    <h3>Submit new ticket</h3>
					<hr>
<?php
//get categories
function getCat(){
        $sql = mysql_query("SELECT * FROM `tickets_cat`");
        while($record = mysql_fetch_array($sql)){
            echo '<option value="' . $record['id'] . '">' . $record['category'] . '</option>'; 
        }
}

if (isset($_POST['submit'])) {
	if (!is_numeric($_POST['cat']) || !isset($_POST['subject']) || !isset($_POST['message'])) {
		$general ->alert('Please fill all required fields!', 'danger');
	} else {
		$uid = $_SESSION["userid"];
		$catID = mysql_real_escape_string(htmlspecialchars($_POST['cat']));
		$subject = mysql_real_escape_string(htmlspecialchars($_POST['subject']));
		$oid = mysql_real_escape_string(htmlspecialchars($_POST['order-id']));
		$message = mysql_real_escape_string(htmlspecialchars($_POST['message']));
		$time = gmdate('ymdHis');
		$username = $_SESSION["username"];

		//insert data
        $sql = "INSERT INTO `tickets` 
        (`uid`,`oid`,`cat_id`,`subject`,`status_id`,`last-activity`,`aread`) 
        VALUES
        ('$uid','$oid','$catID','$subject','1','$time','1')";

        if (mysql_query($sql, $conn)) {
		    $last_id = mysql_insert_id($conn);
		} else {
			die('Could not enter data: ' . mysql_error($conn));
		}

		$sql2 = "INSERT INTO `tickets_replies` 
        (`uid`,`reply_by`,`tid`,`reply_msg`,`time`) 
        VALUES
        ('$uid','$username','$last_id','$message' ,'$time')";

        //test
        $retval = mysql_query( $sql2, $conn );
        if(! $retval )
        {
          die('Could not enter data: ' . mysql_error());
        }
		$general ->alert('Ticket ID: <b>#' . $last_id . '</b></a> Successfully Submitted! <a href="tickets.php"><i class="fa fa-undo"></i> Back to Tickets</a>', 'success');
	}
}

?>
					<form method="post" action="ticket-submit.php">
						<div class="form-group">
							<label for="cat">Category:<span class="red">*</span></label>
				            <select class="form-control" id="cat" name="cat" required>
				                <option value="" selected hidden>Select Category...</option>
				                <?php getCat(); ?>
				            </select>

						</div>

						<div class="form-group">
							<label for="subject">Subject:<span class="red">*</span></label>
							<input type="text" class="form-control" maxlength="100" id="subject" name="subject" placeholder="Subject..." required>
						</div>
						<div class="form-group">
							<label for="order-id">Order ID:</label>
							<input type="text" class="form-control" id="order-id" maxlength="10" name="order-id" placeholder="Order ID...">
						</div>
						<div class="form-group">
							<label for="message">Message:<span class="red">*</span></label>
							<textarea class="form-control"  rows="6" id="message" name="message" placeholder="Message..." required></textarea>
						</div>
						<input class="btn btn-primary" type="submit" name="submit" value="Submit New Ticket">
						<a class="btn btn-default no-radius" href="tickets.php"><i class="fa fa-undo"></i> Back to Tickets</a>
					</form>
                    <br />
                    <small>Note: all fields with <span class="red">*</span> are required!</small>

    			</div>
    		</div>
    	</div>
    </div>
</div>

<?php
include 'includes/footer.php';
?>